<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 3/19/2019
 * Time: 11:25 PM
 */ ?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Lacasa Kopi Jember</title>
    <link rel="shortcut icon" href="<?=base_url()?>assets/assets/images/LOGO LA CASA.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="<?php echo base_url()?>assets/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/assets/libs/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <style>
html,
    body {
    height: 100%;
    background-image:url(<?=base_url()?>assets/assets/images/lacasa1.jpg);
    background-size:cover;
    background-attachment: fixed;
    background-position: center;
}

    body {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- login page  -->
    <!-- ============================================================== -->
    <div class="splash-container">
        <div class="card ">
            <div class="card-header text-center">
                <a href="<?php echo base_url()?>"><img class="logo-img" src="<?php echo base_url()?>assets/assets/images/LOGO LA CASA.png" alt="logo" style="width: 200px"></a>

                <h1 style="margin-top: 20px">Sistem Peramalan Penjualan Kopi</h1>
            </div>
            <div class="card-body">
                <form action="<?=site_url('login/aksi_login')?>" method="post">
                    <div class="form-group">
                        <input class="form-control form-control-lg" id="username" type="text" placeholder="Username" name="username" required>
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" id="password" type="password" placeholder="Password" name="password" required>
                    </div>
<!--                    <div class="form-group">-->
<!--                        <label class="custom-control custom-checkbox">-->
<!--                            <input class="custom-control-input" type="checkbox"><span class="custom-control-label">Remember Me</span>-->
<!--                        </label>-->
<!--                    </div>-->
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Masuk</button>
                </form>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- end login page  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="<?php echo base_url()?>assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url()?>assets/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
</body>

</html>