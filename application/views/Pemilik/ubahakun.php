<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/12/2019
 * Time: 11:29 PM
 */ ?>


<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-uppercase">Akun</h2>
                    <hr>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12">
                <!-- ============================================================== -->
                <!-- card profile -->
                <!-- ============================================================== -->
                <div class="card">
                    <div class="card-body">
                        <div class="user-avatar text-center d-block">
                            <img src="<?=base_url()?>assets/FotoProfil/<?=$akun['foto']?>" alt="User Avatar" class="rounded-circle user-avatar-xxl">
                        </div>
                        <div class="text-center">
                            <h2 class="font-24 mb-0"><?=$akun['nama']?></h2>
                            <p><?php if ($akun['jabatan'] == 1){echo "Pemilik";}elseif($akun['jabatan'] == 2){echo "Pegawai";}?> @LacasaKopi</p>
                        </div>
                    </div>
                    <div class="card-body border-top">
                        <h3 class="font-16">Informasi Akun</h3>
                        <div class="">
                            <ul class="list-unstyled mb-0">
                                <li class="mb-2"><i class="fas fa-fw fa-user mr-2"></i><?=$akun['username']?></li>
                                <li class="mb-0"><i class="fas fa-fw fa-lock mr-2"></i><?=$akun['password']?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body border-top">
                        <h3 class="font-16">Hak Akses</h3>
                        <div>
                            <?php if ($akun['penjualan'] == 1){ ?>
                                <a href="#" class="badge badge-light mr-1">Penjualan</a>
                            <?php } ?>
                            <?php if ($akun['keuangan'] == 1){ ?>
                                <a href="#" class="badge badge-light mr-1">Keuangan</a>
                            <?php } ?>
                            <?php if ($akun['stok'] == 1){ ?>
                                <a href="#" class="badge badge-light mr-1">Stok</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end card profile -->
                <!-- ============================================================== -->
            </div>

            <div class="col-xl-8 col-lg-8 col-md-5 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header">Ubah Akun</h5>
                    <div class="card-body">
                        <form action="<?=site_url('Pemilik/ubahAkun')?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                            <div class="row">
                                <div class="offset-xl-3 col-xl-6 offset-lg-3 col-lg-3 col-md-12 col-sm-12 col-12 p-4">
                                    <div class="form-group">
                                        <label for="name">Username</label>
                                        <input type="text" class="form-control" name="username" value="<?=$akun['username']?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" name="password" value="<?=$akun['password']?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="subject">Nama</label>
                                        <input type="text" class="form-control" name="nama" value="<?=$akun['nama']?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="messages">Foto</label>
                                        <input type="file" class="form-control" name="gambar">
                                        <input type="hidden" class="form-control" name="gambar_lama" value="<?=$akun['foto']?>">
                                        <input type="hidden" class="form-control" name="id" value="<?=$akun['id']?>">
                                    </div>
                                    <button type="submit" class="btn btn-primary float-right">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
