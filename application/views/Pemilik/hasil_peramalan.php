<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 6/17/2019
 * Time: 10:25 PM
 */ ?>


<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-uppercase">Hasil Peramalan</h2>
                    <hr>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <h5 class="mb-0">Rekomendasi Hasil Peramalan dengan MAPE Terkecil</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="" class="table table-hover" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Periode</th>
                                    <th>Metode</th>
                                    <th>Hasil</th>
                                    <th>PE</th>
                                    <th>MAPE</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th><?=$hasil_rekomendasi[$indexMapeTerkecil]['periode']." - ".date('Y-m-d', strtotime('+6 day', strtotime($hasil_rekomendasi[$indexMapeTerkecil]['periode'])))?></th>
                                        <th><?=$hasil_rekomendasi[$indexMapeTerkecil]['metode']?></th>
                                        <th><?=$hasil_rekomendasi[$indexMapeTerkecil]['hasil']?></th>
                                        <th><?=$hasil_rekomendasi[$indexMapeTerkecil]['PE']?></th>
                                        <th><?=$hasil_rekomendasi[$indexMapeTerkecil]['MAPE']?></th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card-body border-top">
                        <h4>Kesimpulan :</h4>
                        <ul>
                            <li>Metode peramalan terbaik adalah <em><strong><?=$hasil_rekomendasi[$indexMapeTerkecil]['metode']?></strong></em> dengan perolehan MAPE <strong><?=$hasil_rekomendasi[$indexMapeTerkecil]['MAPE']?></strong></li>
                            <li>Hasil peramalan penjualan adalah <?=number_format($hasil_rekomendasi[$indexMapeTerkecil]['hasil'], '0')?></li>
                            <li>Hasil perkiraan penyediaan stok <strong><?=$bahanbaku['nama_bahanbaku']?></strong> adalah <?=number_format($hasil_rekomendasi[$indexMapeTerkecil]['hasil'], '0')." * ".$bahanbaku['takaran'].$bahanbaku['satuan']." = "?> <?=$stok = number_format($hasil_rekomendasi[$indexMapeTerkecil]['hasil'], '0')*$bahanbaku['takaran']?> <?=$bahanbaku['satuan']?></li>
                        </ul>
                    </div>
                    <div class="card-body border-top text-right">
                        <form method="post" action="<?=site_url('Pemilik/simpanPeramalan')?>">
                            <a href="<?=site_url('Pemilik/buat_peramalan')?>" class="btn btn-danger">Kembali</a>
                            <input type="hidden" name="bahanbaku_id" value="<?=$bahanbaku['id_bahanbaku']?>">
                            <input type="hidden" name="tanggal" value="<?=date('Y-m-d')?>">
                            <input type="hidden" name="periode" value="<?=$hasil_rekomendasi[$indexMapeTerkecil]['periode']?>">
                            <input type="hidden" name="metode" value="<?=$hasil_rekomendasi[$indexMapeTerkecil]['metode']?>">
                            <input type="hidden" name="hasil" value="<?=$hasil_rekomendasi[$indexMapeTerkecil]['hasil']?>">
                            <input type="hidden" name="rekomendasi_stok" value="<?=$stok?>">
                            <input type="hidden" name="MAPE" value="<?=$hasil_rekomendasi[$indexMapeTerkecil]['MAPE']?>">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <h5 class="mb-0">Detail Hasil Perhitungan <em>Single Exponential Smoothing</em> dan Regresi Linier Sederhana</h5>
                    </div>
                    <div class="card-body">
                        <div class="accrodion-regular">
                            <div id="accordion3">
                                <div class="card mb-2">
                                    <div class="card-header" id="headingEight">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                                <span class="fas fa-angle-down mr-3"></span><?=$ses[0]['metode']?>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion3">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table id="ses" class="table table-hover" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Periode</th>
                                                        <th>Aktual</th>
                                                        <th>Hasil</th>
                                                        <th>PE</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($ses as $s){ ?>
                                                        <tr>
                                                            <th><?=$s['periode']." - ".date('Y-m-d', strtotime('+6 day', strtotime($s['periode'])))?></th>
                                                            <th><?=$s['aktual']?></th>
                                                            <th><?=$s['hasil']?></th>
                                                            <th><?=$s['PE']?></th>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                    <tfoot class="bg-gray">
                                                    <tr>
                                                        <th class="text-center" colspan="3">MAPE</th>
                                                        <th><?=$s['MAPE']?></th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-2">
                                    <div class="card-header" id="headingNine">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                                <span class="fas fa-angle-down mr-3"></span><?=$regresi[0]['metode']?>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion3">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table id="ses" class="table table-hover" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Periode</th>
                                                        <th>Aktual</th>
                                                        <th>Hasil</th>
                                                        <th>PE</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($regresi as $r){ ?>
                                                        <tr>
                                                            <th><?=$r['periode']." - ".date('Y-m-d', strtotime('+6 day', strtotime($r['periode'])))?></th>
                                                            <th><?=$r['aktual']?></th>
                                                            <th><?=$r['hasil']?></th>
                                                            <th><?=$r['PE']?></th>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                    <tfoot class="bg-gray">
                                                    <tr>
                                                        <th class="text-center" colspan="3">MAPE</th>
                                                        <th><?=$r['MAPE']?></th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
