<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/12/2019
 * Time: 11:29 PM
 */ ?>


<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-uppercase">Stok Bahanbaku</h2>
                    <hr>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <h5 class="mb-0">Daftar Stok Bahanbaku</h5>
                        <a class="btn btn-sm btn-primary text-light ml-auto w-auto" data-toggle="modal" data-target="#tambahbahanbaku"><i class="fa fa-plus"> Bahanbaku</i></a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered js-basic-example dataTable" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Bahanbaku</th>
                                    <th>Harga Per Satuan</th>
                                    <th>Satuan</th>
                                    <th>Stok</th>
                                    <th>Rekomendasi Stok</th>
                                    <th>Jenis Bahanbaku</th>
                                    <th style="width: 90px">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $no =1;
                                    foreach ($bahanbaku as $bb){
                                ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=$bb['nama_bahanbaku']?></td>
                                    <td>Rp. <?=number_format($bb['harga_bahanbaku'], '0', '.', '.')?></td>
                                    <td><?=$bb['satuan']?></td>
                                    <td><?=$bb['stok']?> gr</td>
                                    <td><?=$bb['rek_stok']?> gr</td>
                                    <td>
                                        <?php
                                        if ($bb['jenis_bahanbaku'] ==1){
                                            echo "Kopi";
                                        }
                                        if ($bb['jenis_bahanbaku'] ==2){
                                            echo "Lainnya";
                                        }
                                        ?>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-xs btn-rounded btn-success text-light tambahStok" data-toggle="tooltip" data-placement="top" data-original-title="Tambah Stok" data-idbahanbaku="<?=$bb['id_bahanbaku']?>" data-namabahanbaku="<?=$bb['nama_bahanbaku']?>" data-satuan="<?=$bb['satuan']?>"><i class="fa fa-plus"></i></a>
                                        <a class="btn btn-xs btn-rounded btn-info text-light editBahanbaku" data-toggle="tooltip" data-placement="top" data-original-title="Edit Bahanbaku" data-idbahanbaku="<?=$bb['id_bahanbaku']?>" data-namabahanbaku="<?=$bb['nama_bahanbaku']?>" data-hargabahanbaku="<?=$bb['harga_bahanbaku']?>" data-satuan="<?=$bb['satuan']?>" data-jenisbahanbaku="<?=$bb['jenis_bahanbaku']?>" ><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-xs btn-rounded btn-danger text-light" data-toggle="tooltip" data-placement="top" data-original-title="Hapus Bahanbaku" onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" href="<?=site_url('Pemilik/hapusBahanbaku/'.$bb['id_bahanbaku'])?>"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="tambahbahanbaku" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Bahanbaku</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pemilik/tambahbahanbaku')?>" method="post" class="needs-validation" novalidate>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="nama">Nama Bahanbaku</label>
                                <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama Bahanbaku" name="nama" required>
                                <div class="invalid-feedback">
                                    Masukkan Nama Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="harga_bahanbaku">Harga Bahanbaku</label>
                                <input type="number" class="form-control" id="harga_bahanbaku" min="0" placeholder="Masukkan harga bahanbaku per satuan" aria-describedby="inputGroupPrepend" name="harga" required>
                                <div class="invalid-feedback">
                                    Masukkan Harga Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="satuan_bahanbaku">Satuan Bahanbaku</label>
                                <input type="text" class="form-control" id="satuan_bahanbaku" placeholder="Masukkan satuan bahanbaku" aria-describedby="inputGroupPrepend" name="satuan" required>
                                <div class="invalid-feedback">
                                    Masukkan Satuan Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="jenis_bahanbaku">Jenis Bahanbaku</label>
                                <select id="jenis_bahanbaku" name="jenis_bahanbaku" class="form-control" required>
                                    <option value="">--- Pilih Jenis Bahanbaku ---</option>
                                    <option value="1">Kopi</option>
                                    <option value="2">Lainnya</option>
                                </select>
                                <div class="invalid-feedback">
                                    Pilih Jenis Bahanbaku Dengan Benar !
                                </div>
                            </div>
<!--                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">-->
<!--                                <label for="stok_bahanbaku">Stok Bahanbaku</label>-->
<!--                                <input type="text" class="form-control" id="stok_bahanbaku" min="0" placeholder="Masukkan stok bahanbaku" aria-describedby="inputGroupPrepend" name="stok" required>-->
<!--                                <div class="invalid-feedback">-->
<!--                                    Masukkan Satuan Dengan Benar !-->
<!--                                </div>-->
<!--                            </div>-->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-secondary" data-dismiss="modal">Batal</a>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editBahanbaku" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Bahanbaku</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pemilik/editbahanbaku')?>" method="post" class="needs-validation" novalidate>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="namabahanbaku">Nama Bahanbaku</label>
                                <input type="text" class="form-control" id="namabahanbaku" placeholder="Masukkan Nama Bahanbaku" name="nama" required>
                                <input type="hidden" class="form-control" id="idbahanbaku" placeholder="Masukkan Nama Bahanbaku" name="id" required>
                                <div class="invalid-feedback">
                                    Masukkan Nama Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="hargabahanbaku">Harga Bahanbaku</label>
                                <input type="number" class="form-control" id="hargabahanbaku" min="0" placeholder="Masukkan harga bahanbaku per satuan" aria-describedby="inputGroupPrepend" name="harga" required>
                                <div class="invalid-feedback">
                                    Masukkan Harga Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="satuanbahanbaku">Satuan Bahanbaku</label>
                                <input type="text" class="form-control" id="satuanbahanbaku" placeholder="Masukkan satuan bahanbaku" aria-describedby="inputGroupPrepend" name="satuan" required>
                                <div class="invalid-feedback">
                                    Masukkan Satuan Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="jenisbahanbaku">Jenis Bahanbaku</label>
                                <select id="jenisbahanbaku" name="jenis_bahanbaku" class="form-control" required>
                                    <option value="">--- Pilih Jenis Bahanbaku ---</option>
                                    <option value="1">Kopi</option>
                                    <option value="2">Lainnya</option>

                                </select>
                                <div class="invalid-feedback">
                                    Pilih Jenis Bahanbaku Dengan Benar !
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-secondary" data-dismiss="modal">Batal</a>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="tambahStok" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Stok : "<span id="nama_bahanbaku"></span>"</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pemilik/tambahStok')?>" method="post" class="needs-validation" novalidate>
                    <div class="modal-body">
                        <div class="row">
                            <input type="hidden" class="form-control" id="id_bahanbaku" placeholder="Masukkan Nama Bahanbaku" name="id_bahanbaku" required>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="tglbeli">Tanggal Beli (Bulan/Hari/Tahun)</label>
                                <input type="date" class="form-control" id="tglbeli" placeholder="Masukkan tanggal beli" aria-describedby="inputGroupPrepend" name="tgl_beli" value="<?=$now=date('Y-m-d')?>" required>
                                <div class="invalid-feedback">
                                    Masukkan Tanggal Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="tglkadaluarsa">Tanggal Kadaluarsa (Bulan/Hari/Tahun)</label>
                                <input type="date" class="form-control" id="tglkadaluarsa" placeholder="Masukkan tanggal kadaluarsa" aria-describedby="inputGroupPrepend" name="tgl_kadaluarsa" value="<?=date('Y-m-d', strtotime('+21 day', strtotime($now)))?>" required>
                                <div class="invalid-feedback">
                                    Masukkan Tanggal Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="stok_bahanbaku">Jumlah Stok (<span id="satuan"></span>)</label>
                                <input type="number" class="form-control" min="1" id="stok_bahanbaku" placeholder="Masukkan jumlah stok bahanbaku" aria-describedby="inputGroupPrepend" name="stok" required>
                                <div class="invalid-feedback">
                                    Masukkan Stok Dengan Benar !
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-secondary" data-dismiss="modal">Batal</a>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="<?=base_url()?>assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.tambahStok').on('click', function () {
                var nama = $(this).data('namabahanbaku');
                var satuan = $(this).data('satuan');

                document.getElementById('nama_bahanbaku').innerHTML=nama;
                document.getElementById('satuan').innerHTML=satuan;
                $('#id_bahanbaku').val($(this).data('idbahanbaku'));
                $('#nama_bahanbaku').val($(this).data('namabahanbaku'));
                $('#tambahStok').modal('show');
            });

            $('.editBahanbaku').on('click', function () {
                $('#idbahanbaku').val($(this).data('idbahanbaku'));
                $('#namabahanbaku').val($(this).data('namabahanbaku'));
                $('#hargabahanbaku').val($(this).data('hargabahanbaku'));
                $('#satuanbahanbaku').val($(this).data('satuan'));
                $('#jenisbahanbaku').val($(this).data('jenisbahanbaku')).change();
                $('#editBahanbaku').modal('show');
                console.log($(this).data('jenisbahanbaku'));
            });
        });
    </script>