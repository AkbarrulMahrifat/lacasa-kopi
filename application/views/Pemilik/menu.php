<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/12/2019
 * Time: 11:29 PM
 */ ?>


<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-uppercase">Manajemen Menu</h2>
                    <hr>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <h5 class="mb-0">Daftar Menu</h5>
                        <a class="btn btn-sm btn-primary text-light ml-auto w-auto tambahMenu" data-toggle="tooltip" data-placement="top" data-original-title="Tambah Menu"><i class="fa fa-plus"></i></a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered js-basic-example dataTable" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Menu</th>
                                    <th>Harga Menu</th>
                                    <th>Gambar</th>
                                    <th style="width: 90px">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $no =1;
                                    foreach ($menu as $m){
                                ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=$m->nama_menu?></td>
                                    <td>Rp. <?=number_format($m->harga, '0', '.', '.')?></td>
                                    <td class="text-center">
                                        <img src="<?=base_url().'assets/FotoMenu/'.$m->gambar?>" style="width: 70px; height: 70px">
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-xs btn-rounded btn-info text-light editMenu" data-toggle="tooltip" data-placement="top" data-original-title="Edit Menu" data-idmenu="<?=$m->id_menu?>" data-namamenu="<?=$m->nama_menu?>" data-hargamenu="<?=$m->harga?>" data-gambarmenu="<?=$m->gambar?>" data-gambar="<?=base_url().'assets/FotoMenu/'.$m->gambar?>"><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-xs btn-rounded btn-danger text-light" data-toggle="tooltip" data-placement="top" data-original-title="Hapus Menu" onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" href="<?=site_url('Pemilik/hapusMenu/'.$m->id_menu)?>"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="tambahMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Menu</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pemilik/tambahMenu')?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="namamenu">Nama Menu</label>
                                <input type="text" class="form-control" id="namamenu" placeholder="Masukkan nama menu" aria-describedby="inputGroupPrepend" name="nama_menu" required>
                                <div class="invalid-feedback">
                                    Masukkan Nama Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="hargamenu">Harga Menu</label>
                                <input type="number" class="form-control" min="1" id="hargamenu" placeholder="Masukkan harga menu" aria-describedby="inputGroupPrepend" name="harga_menu" required>
                                <div class="invalid-feedback">
                                    Masukkan Harga Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="gambarmenu">Gambar Menu</label>
                                <input type="file" class="form-control" id="gambarmenu" placeholder="Upload gambar menu" aria-describedby="inputGroupPrepend" name="gambar" required>
                                <div class="invalid-feedback">
                                    Upload Gambar Dengan Benar !
                                </div>
                            </div>

                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 input_fields_wrap">
                                <label for="resep">Resep</label>
                                <div class="input-group mb-3">
                                    <select class="form-control" id="resep" aria-describedby="inputGroupPrepend" name="resep[]" required>
                                        <option value="">--- Pilih Bahanbaku ---</option>
                                        <?php foreach ($bahanbaku as $bb){?>
                                            <option value="<?=$bb->id_bahanbaku?>"><?=$bb->nama_bahanbaku?></option>
                                        <?php } ?>
                                    </select>
                                    <input type="number" class="form-control" min="1" id="takaran" placeholder="Masukkan takaran resep" aria-describedby="inputGroupPrepend" name="takaran[]" required>
                                    <a href="#" class="input-group-text input-group-append"><i class="fa fa-window-close" aria-disabled="true"></i></a>
                                    <div class="invalid-feedback">
                                        Masukkan Resep dan Takaran Dengan Benar !
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <button class="add_field_button btn btn-xs btn-success"><i class="fa fa-plus"></i> Tambah Form Resep</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="reset">Reset</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Menu</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pemilik/updateMenu')?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                    <input type="hidden" name="id_menu" id="id_menu">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="nama_menu">Nama Menu</label>
                                <input type="text" class="form-control" id="nama_menu" placeholder="Masukkan nama menu" aria-describedby="inputGroupPrepend" name="nama_menu" required>
                                <div class="invalid-feedback">
                                    Masukkan Nama Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="harga_menu">Harga Menu</label>
                                <input type="number" class="form-control" min="1" id="harga_menu" placeholder="Masukkan harga menu" aria-describedby="inputGroupPrepend" name="harga_menu" required>
                                <div class="invalid-feedback">
                                    Masukkan Harga Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="gambar">Gambar Menu</label>
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend"><span class="input-group-text">Gambar Lama</span></div>
                                    <input type="text" id="gambar" class="form-control" placeholder="Upload gambar menu" aria-describedby="inputGroupPrepend" name="gambarlama" readonly>
                                </div>
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend"><span class="input-group-text">Gambar Baru</span></div>
                                    <input type="file" class="form-control" placeholder="Upload gambar menu" aria-describedby="inputGroupPrepend" name="gambar">
                                </div>
                                <div class="invalid-feedback">
                                    Upload Gambar Dengan Benar !
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="reset">Reset</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="<?=base_url()?>assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.tambahMenu').on('click', function () {
                $('#tambahMenu').modal('show');
            });

            $('.editMenu').on('click', function () {
                var img = $(this).data('gambarmenu');
                $('#id_menu').val($(this).data('idmenu'));
                $('#nama_menu').val($(this).data('namamenu'));
                $('#harga_menu').val($(this).data('hargamenu'));
                $('#gambar').val($(this).data('gambarmenu'));
                // document.getElementById('gambar').src = img;
                $('#editMenu').modal('show');
            });
        });

        $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('' +
                        '<div class="input-group mb-3">' +
                            '<select type="number" class="form-control" id="resep" aria-describedby="inputGroupPrepend" name="resep[]" required>' +
                            '<option value="">--- Pilih Bahanbaku ---</option>' +
                            '<?php foreach ($bahanbaku as $bb){?>\n' +
                            '<option value="<?=$bb->id_bahanbaku?>"><?=$bb->nama_bahanbaku?></option>\n' +
                            '<?php } ?>' +
                            '</select>' +
                            '<input type="number" class="form-control" min="1" id="takaran" placeholder="Masukkan takaran resep" aria-describedby="inputGroupPrepend" name="takaran[]" required>' +
                            '<a href="#" class="remove_field input-group-text input-group-append"><i class="fa fa-window-close"></i></a>' +
                            '<div class="invalid-feedback">' +
                                'Masukkan Resep dan Takaran Dengan Benar !' +
                            '</div>' +
                        '</div>'
                        ); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });
    </script>