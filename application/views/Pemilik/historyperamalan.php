<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 6/19/2019
 * Time: 10:53 PM
 */ ?>


<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-uppercase">Peramalan</h2>
                    <hr>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <h5 class="mb-0">Riwayat Peramalan</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered js-basic-example dataTable" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Bahanbaku</th>
                                    <th>Tanggal</th>
                                    <th>Periode</th>
                                    <th>Metode</th>
                                    <th>Hasil</th>
                                    <th>MAPE</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $no =1;
                                foreach ($peramalan as $p){
                                    ?>
                                    <tr>
                                        <td><?=$no++?></td>
                                        <td><?=$p->nama_bahanbaku?></td>
                                        <td><?=$p->tanggal?></td>
                                        <td><?=$p->periode?></td>
                                        <td><?=$p->metode?></td>
                                        <td><?=$p->hasil?></td>
                                        <td><?=$p->MAPE?></td>
                                        <td class="text-center">
                                            <a class="btn btn-xs btn-rounded btn-danger text-light" data-toggle="tooltip" data-placement="top" data-original-title="Hapus Peramalan" onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" href="<?=site_url('Pemilik/hapusPeramalan/'.$p->id_peramalan)?>"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
