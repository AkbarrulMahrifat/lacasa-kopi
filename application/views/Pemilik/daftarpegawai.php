<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/12/2019
 * Time: 11:29 PM
 */ ?>


<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-uppercase">Manajemen Pegawai</h2>
                    <hr>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <h5 class="mb-0">Daftar Pegawai</h5>
                        <a class="btn btn-sm btn-primary text-light ml-auto w-auto tambahPegawai" data-toggle="tooltip" data-placement="top" data-original-title="Tambah Pegawai"><i class="fa fa-plus"></i></a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered js-basic-example dataTable" style="width:100%">
                                <thead class="text-center">
                                <tr>
                                    <th rowspan="2">#</th>
                                    <th rowspan="2">Nama Pegawai</th>
                                    <th rowspan="2">Username</th>
                                    <th rowspan="2">Password</th>
                                    <th colspan="3">Hak Akses</th>
                                    <th rowspan="2">Foto</th>
                                    <th rowspan="2" style="width: 90px">Aksi</th>
                                </tr>
                                <tr>
                                    <th>Penjualan</th>
                                    <th>Keuangan</th>
                                    <th>Stok</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $no =1;
                                    foreach ($pegawai as $p){
                                ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=$p->nama?></td>
                                    <td><?=$p->username?></td>
                                    <td><?=$p->password?></td>
                                    <td class="text-center">
                                        <?php if ($p->penjualan == 1){?>
                                            <span class="badge badge-success"><i class="fa fa-fw fa-check"></i></span>
                                        <?php }else{ ?>
                                            <span class="badge badge-danger"><i class="fa fa-fw fa-times"></i></span>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if ($p->keuangan == 1){?>
                                            <span class="badge badge-success"><i class="fa fa-fw fa-check"></i></span>
                                        <?php }else{ ?>
                                            <span class="badge badge-danger"><i class="fa fa-fw fa-times"></i></span>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if ($p->stok == 1){?>
                                            <span class="badge badge-success"><i class="fa fa-fw fa-check"></i></span>
                                        <?php }else{?>
                                            <span class="badge badge-danger"><i class="fa fa-fw fa-times"></i></span>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center">
                                        <img src="<?=base_url().'assets/FotoProfil/'.$p->foto?>" style="width: 70px; height: 70px">
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-xs btn-rounded btn-info text-light editPegawai" data-toggle="tooltip" data-placement="top" data-original-title="Edit Pegawai" data-id="<?=$p->id?>" data-nama="<?=$p->nama?>" data-username="<?=$p->username?>" data-password="<?=$p->password?>" data-penjualan="<?=$p->penjualan?>" data-keuangan="<?=$p->keuangan?>" data-stok="<?=$p->stok?>" data-foto="<?=$p->foto?>"><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-xs btn-rounded btn-danger text-light" data-toggle="tooltip" data-placement="top" data-original-title="Hapus Pegawai" onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" href="<?=site_url('Pemilik/hapusPegawai/'.$p->id)?>"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="tambahPegawai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Pegawai</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pemilik/tambahPegawai')?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="nama">Nama Pegawai</label>
                                <input type="text" class="form-control" id="nama" placeholder="Masukkan nama pegawai" aria-describedby="inputGroupPrepend" name="nama" required>
                                <div class="invalid-feedback">
                                    Masukkan Nama Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" id="username" placeholder="Masukkan username pegawai" aria-describedby="inputGroupPrepend" name="username" required>
                                <div class="invalid-feedback">
                                    Masukkan Username Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="password">Password</label>
                                <input type="text" class="form-control" id="password" placeholder="Masukkan password pegawai" aria-describedby="inputGroupPrepend" name="password" required>
                                <div class="invalid-feedback">
                                    Masukkan Password Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <label for="password">Hak Akses</label>
                                <div class="form-group row" style="padding-top: 0px">
                                    <label class="col-12 col-sm-2 col-form-label text-sm-right">Penjualan</label>
                                    <div class="col-12 col-sm-2 col-lg-2 pt-1" style="padding-left: 2px">
                                        <div class="switch-button switch-button-xs switch-button-success">
                                            <input type="checkbox" name="penjualan" id="penjualan"><span>
                                                    <label for="penjualan"></label></span>
                                        </div>
                                    </div>

                                    <label class="col-12 col-sm-2 col-form-label text-sm-right">Keuangan</label>
                                    <div class="col-12 col-sm-2 col-lg-2 pt-1" style="padding-left: 2px">
                                        <div class="switch-button switch-button-xs switch-button-success">
                                            <input type="checkbox" name="keuangan" id="keuangan"><span>
                                                    <label for="keuangan"></label></span>
                                        </div>
                                    </div>

                                    <label class="col-12 col-sm-2 col-form-label text-sm-right">Stok</label>
                                    <div class="col-12 col-sm-2 col-lg-2 pt-1" style="padding-left: 2px">
                                        <div class="switch-button switch-button-xs switch-button-success">
                                            <input type="checkbox" name="stok" id="stok"><span>
                                                    <label for="stok"></label></span>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="foto">Foto Pegawai</label>
                                <input type="file" class="form-control" id="foto" placeholder="Upload foto pegawai" aria-describedby="inputGroupPrepend" name="gambar" required>
                                <div class="invalid-feedback">
                                    Upload Foto Dengan Benar !
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="reset">Reset</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="editPegawai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Pegawai</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pemilik/ubahPegawai')?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="nama">Nama Pegawai</label>
                                <input type="hidden" class="form-control" id="id_pegawai" placeholder="Masukkan nama pegawai" aria-describedby="inputGroupPrepend" name="id" required>
                                <input type="text" class="form-control" id="nama_pegawai" placeholder="Masukkan nama pegawai" aria-describedby="inputGroupPrepend" name="nama" required>
                                <div class="invalid-feedback">
                                    Masukkan Nama Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" id="username_pegawai" placeholder="Masukkan username pegawai" aria-describedby="inputGroupPrepend" name="username" required>
                                <div class="invalid-feedback">
                                    Masukkan Username Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="password">Password</label>
                                <input type="text" class="form-control" id="password_pegawai" placeholder="Masukkan password pegawai" aria-describedby="inputGroupPrepend" name="password" required>
                                <div class="invalid-feedback">
                                    Masukkan Password Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <label for="password">Hak Akses</label>
                                <div class="form-group row" style="padding-top: 0px">
                                    <label class="col-12 col-sm-2 col-form-label text-sm-right">Penjualan</label>
                                    <div class="col-12 col-sm-2 col-lg-2 pt-1" style="padding-left: 2px">
                                        <div class="switch-button switch-button-xs switch-button-success">
                                            <input type="checkbox" name="penjualan" id="penjualan_pegawai"><span>
                                                    <label for="penjualan_pegawai"></label></span>
                                        </div>
                                    </div>

                                    <label class="col-12 col-sm-2 col-form-label text-sm-right">Keuangan</label>
                                    <div class="col-12 col-sm-2 col-lg-2 pt-1" style="padding-left: 2px">
                                        <div class="switch-button switch-button-xs switch-button-success">
                                            <input type="checkbox" name="keuangan" id="keuangan_pegawai"><span>
                                                    <label for="keuangan_pegawai"></label></span>
                                        </div>
                                    </div>

                                    <label class="col-12 col-sm-2 col-form-label text-sm-right">Stok</label>
                                    <div class="col-12 col-sm-2 col-lg-2 pt-1" style="padding-left: 2px">
                                        <div class="switch-button switch-button-xs switch-button-success">
                                            <input type="checkbox" name="stok" id="stok_pegawai"><span>
                                                    <label for="stok_pegawai"></label></span>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="foto">Foto Pegawai</label>
                                <input type="file" class="form-control" id="foto" placeholder="Upload foto pegawai" aria-describedby="inputGroupPrepend" name="gambar">
                                <input type="hidden" class="form-control" id="foto_pegawai" placeholder="Upload foto pegawai" aria-describedby="inputGroupPrepend" name="gambar_lama" required>
                                <div class="invalid-feedback">
                                    Upload Foto Dengan Benar !
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="<?=base_url()?>assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.tambahPegawai').on('click', function () {
                $('#tambahPegawai').modal('show');
            });

            $('.editPegawai').on('click', function () {
                var img = $(this).data('foto');
                var penjualan = $(this).data('penjualan');
                var keuangan = $(this).data('keuangan');
                var stok = $(this).data('stok');
                $('#id_pegawai').val($(this).data('id'));
                $('#nama_pegawai').val($(this).data('nama'));
                $('#username_pegawai').val($(this).data('username'));
                $('#password_pegawai').val($(this).data('password'));
                $('#foto_pegawai').val($(this).data('foto'));

                if (penjualan === 1){
                    $('#penjualan_pegawai').attr("checked", true);
                }
                if (keuangan === 1){
                    $('#keuangan_pegawai').attr("checked", true);
                }
                if (stok === 1){
                    $('#stok_pegawai').attr("checked", true);
                }

                $('#editPegawai').modal('show');
            });
        });

    </script>