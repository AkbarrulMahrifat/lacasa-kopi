<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/12/2019
 * Time: 11:26 PM
 */ ?>

<!doctype html>
<html lang="en">


<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Lacasa Kopi Jember</title>
    <link rel="shortcut icon" href="<?=base_url()?>assets/assets/images/LOGO LA CASA.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?=base_url()?>assets/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="<?=base_url()?>assets/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url()?>assets/assets/libs/css/style.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">

    <!-- JQuery DataTable Css -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/assets/vendor/datatables/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/assets/vendor/datatables/css/buttons.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/assets/vendor/datatables/css/select.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/assets/vendor/datatables/css/fixedHeader.bootstrap4.css">

    <!-- Sweetalert Css -->
    <link href="<?=base_url()?>assets/assets/vendor/sweetalert/sweetalert.css" rel="stylesheet" /></head>

    <script src="<?=base_url()?>assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
       <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top">
                <a class="navbar-brand" href="../index.html">Lacasa Kopi Jember</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        <li class="nav-item dropdown notification">
                            <a class="nav-link nav-icons" href="#" id="navbarDropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-fw fa-bell"></i> <span id="jumlahNotif"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right notification-dropdown">
                                <li>
                                    <div class="notification-title"> Notifikasi</div>
                                    <div class="notification-list">
                                        <div class="list-group" id="notif">

                                            <div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?=$this->session->userdata('nama')?></span>
                                <img src="<?=base_url()?>assets/FotoProfil/<?=$this->session->userdata('foto')?>" alt="" class="user-avatar-md rounded-circle">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                                <div class="nav-user-info">
                                    <h5 class="mb-0 text-white nav-user-name"><?=$this->session->userdata('nama')?></h5>
                                    <span class="status"></span><span class="ml-2"><?php if($this->session->userdata('jabatan')==1){echo "Pemilik";} else{echo "Pegawai";}?></span>
                                </div>
                                <?php if ($this->session->userdata('jabatan') == 1){ ?>
                                    <a class="dropdown-item" href="<?=site_url('Pemilik/ubah_akun')?>"><i class="fas fa-user mr-2"></i>Account</a>
                                <?php } ?>
                                <?php if ($this->session->userdata('jabatan') == 2){ ?>
                                    <a class="dropdown-item" href="<?=site_url('Pegawai/ubah_akun')?>"><i class="fas fa-user mr-2"></i>Account</a>
                                <?php } ?>
                                <a class="dropdown-item" href="<?=site_url('Login/logout')?>"><i class="fas fa-power-off mr-2"></i>Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
      <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">Main Menu</li>
                            <?php if ($this->session->userdata('jabatan') == 1){ ?>
                            <li class="nav-item ">
                                    <a class="nav-link <?php if($this->uri->segment(2) == "beranda"){echo "active";}?>" href="<?=site_url('Pemilik/beranda')?>"><i class="fa fa-fw fa-home"></i>Beranda </a>

<!--                                --><?php //if ($this->session->userdata('jabatan') == 2){ ?>
<!--                                    <a class="nav-link --><?php //if($this->uri->segment(2) == "beranda"){echo "active";}?><!--" href="--><?//=site_url('Pegawai/beranda')?><!--"><i class="fa fa-fw fa-home"></i>Beranda </a>-->
<!--                                --><?php //} ?>
                            </li>
                            <?php } ?>

                            <?php if ($this->session->userdata('penjualan') == 1){ ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-2"><i class="fa fa-fw fa-money-bill-alt"></i>Penjualan </a>
                                    <div id="submenu-2" class="collapse submenu" style="">
                                        <ul class="nav flex-column">
                                            <?php if ($this->session->userdata('jabatan') == 1){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pemilik/kasir')?>">Kasir</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pemilik/daftarpenjualan')?>">Riwayat Penjualan</a>
                                                </li>
                                            <?php } ?>

                                            <?php if ($this->session->userdata('jabatan') == 2){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pegawai/kasir')?>">Kasir</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pegawai/daftarpenjualan')?>">Daftar Penjualan</a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </li>
                            <?php } ?>

                            <?php if ($this->session->userdata('keuangan') == 1){ ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#transaksi" aria-controls="transaksi"><i class="fa fa-fw fa-dollar-sign"></i>Rekap Transaksi </a>
                                    <div id="transaksi" class="collapse submenu" style="">
                                        <ul class="nav flex-column">
                                            <?php if ($this->session->userdata('jabatan') == 1){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pemilik/transaksi_masuk')?>">Rekap Transaksi Masuk</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pemilik/transaksi_keluar')?>">Rekap Transaksi Keluar</a>
                                                </li>
                                            <?php } ?>

                                            <?php if ($this->session->userdata('jabatan') == 2){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pegawai/transaksi_masuk')?>">Rekap Transaksi Masuk</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pegawai/transaksi_keluar')?>">Rekap Transaksi Keluar</a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#pengeluaran" aria-controls="pengeluaran"><i class="fa fa-fw fa-credit-card"></i>Tagihan & Pengeluaran </a>
                                    <div id="pengeluaran" class="collapse submenu" style="">
                                        <ul class="nav flex-column">
                                            <?php if ($this->session->userdata('jabatan') == 1){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pemilik/tagihan')?>">Daftar Tagihan</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pemilik/pengeluaran')?>">Daftar Pengeluaran</a>
                                                </li>
                                            <?php } ?>

                                            <?php if ($this->session->userdata('jabatan') == 2){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pegawai/tagihan')?>">Daftar Tagihan</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pegawai/pengeluaran')?>">Daftar Pengeluaran</a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </li>
                            <?php } ?>

                            <?php if ($this->session->userdata('penjualan') == 1){ ?>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#manajemenmenu" aria-controls="manajemenmenu"><i class="fa fa-fw fa-coffee"></i>Manajemen Menu </a>
                                <div id="manajemenmenu" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        <?php if ($this->session->userdata('jabatan') == 1){ ?>
                                            <li class="nav-item">
                                                <a class="nav-link" href="<?=site_url('Pemilik/menu')?>">Daftar Menu</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="<?=site_url('Pemilik/resep')?>">Daftar Resep</a>
                                            </li>
                                        <?php } ?>
                                        <?php if ($this->session->userdata('jabatan') == 2){ ?>
                                            <li class="nav-item">
                                                <a class="nav-link" href="<?=site_url('Pegawai/menu')?>">Daftar Menu</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="<?=site_url('Pegawai/resep')?>">Daftar Resep</a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </li>
                            <?php } ?>

                            <?php if ($this->session->userdata('stok') == 1){ ?>
                                <li class="nav-item">
                                    <a class="nav-link <?php if($this->uri->segment(2) == "stok" or $this->uri->segment(2) == "riwayat_stok"){echo "active";}?>" href="#" data-toggle="collapse" aria-expanded="false" data-target="#menustok" aria-controls="menustok"><i class="fa fa-fw fa-cubes"></i>Manajemen Stok </a>
                                    <div id="menustok" class="collapse submenu" style="">
                                        <ul class="nav flex-column">
                                            <?php if ($this->session->userdata('jabatan') == 1){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pemilik/stok')?>">Stok Bahanbaku</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pemilik/riwayat_stok')?>">Riwayat Stok</a>
                                                </li>
                                            <?php } ?>

                                            <?php if ($this->session->userdata('jabatan') == 2){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pegawai/kasir')?>">Kasir</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pegawai/daftarpenjualan')?>">Daftar Penjualan</a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#peramalan" aria-controls="peramalan"><i class="fa fa-fw fa-chart-line"></i>Peramalan </a>
                                    <div id="peramalan" class="collapse submenu" style="">
                                        <ul class="nav flex-column">
                                            <?php if ($this->session->userdata('jabatan') == 1){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pemilik/buat_peramalan')?>">Buat Peramalan</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pemilik/riwayat_peramalan')?>">Riwayat Peramalan</a>
                                                </li>
                                            <?php } ?>

                                            <?php if ($this->session->userdata('jabatan') == 2){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pegawai/buat_peramalan')?>">Buat Peramalan</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pegawai/riwayat_peramalan')?>">Riwayat Peramalan</a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </li>
                            <?php } ?>

                            <?php if ($this->session->userdata('jabatan') == 1){ ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#akun" aria-controls="akun"><i class="fa fa-fw fa-users"></i>Manajemen Pegawai </a>
                                    <div id="akun" class="collapse submenu" style="">
                                        <ul class="nav flex-column">
                                            <?php if ($this->session->userdata('jabatan') == 1){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="<?=site_url('Pemilik/daftar_pegawai')?>">Daftar Pegawai</a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </li>
                            <?php } ?>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
