<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/12/2019
 * Time: 11:26 PM
 */ ?>

            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
Copyright © 2019 Lacasa Kopi Jember. All rights reserved.
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="text-md-right footer-links d-none d-sm-block">
<!--                                <a href="javascript: void(0);">About</a>-->
<!--                                <a href="javascript: void(0);">Support</a>-->
<!--                                <a href="javascript: void(0);">Contact Us</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end main wrapper -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
<!--    <script src="--><?//=base_url()?><!--assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>-->
    <script src="<?=base_url()?>assets/assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="<?=base_url()?>assets/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <script src="<?=base_url()?>assets/assets/libs/js/main-js.js"></script>
    <script src="<?=base_url()?>assets/assets/vendor/multi-select/js/jquery.multi-select.js"></script>

    <script src="<?=base_url()?>assets/assets/vendor/datatables/js/data-table.js"></script>


<!-- Jquery DataTable Plugin Js -->
<script src="<?=base_url()?>assets/assets/vendor/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?=base_url()?>assets/assets/vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/assets/vendor/datatables/js/buttons.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/assets/vendor/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>assets/assets/vendor/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?=base_url()?>assets/assets/vendor/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?=base_url()?>assets/assets/vendor/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?=base_url()?>assets/assets/vendor/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?=base_url()?>assets/assets/vendor/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?=base_url()?>assets/assets/vendor/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script src="<?=base_url()?>assets/assets/vendor/jquery-datatable/dataTable-example.js"></script>
<script src="<?=base_url()?>assets/assets/vendor/parsley/parsley.js"></script>

<!-- SweetAlert Plugin Js -->
<script src="<?=base_url()?>assets/assets/vendor/sweetalert/sweetalert.min.js"></script>

<script>
    $('#form').parsley();

    $(document).ready(function() {
        <?php if ($this->session->flashdata('success')){?>
            var msg = "<?=$this->session->flashdata('success')?>";
            var type = "success";
            var jdl = "Sukses!";
            showMessage(jdl, msg, type);
        <?php } ?>

        <?php if ($this->session->flashdata('error')){?>
            var msg = "<?=$this->session->flashdata('error')?>";
            var type = "error";
            var jdl = "Gagal!";
            showMessage(jdl, msg, type);
        <?php } ?>

        function showMessage(jdl, msg, type) {
            swal({
                title: jdl,
                text: msg,
                type: type,
                timer: 5000
            });
        }
    });
</script>
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>

<script>
    $(document).ready(function() {
        function notif()
        {
            $.ajax({
                url:"<?=site_url('Notif/getNotif')?>",
                method:"GET",
                // dataType:"json",
                success:function(data)
                {
                    var a = JSON.parse(data);
                    var count = a.length;
                    var b = "<span class=\"indicator\"></span>";
                    $('#notif').html(a);
                    if (count > 0){
                        $('#jumlahNotif').html(b);
                    }
                }
            });
        }

        function ubahStatus()
        {
            $.ajax({
                url:"<?=site_url('Notif/ubahStatusOtomatis')?>",
                method:"POST",
                // dataType:"json",
                success:function(data)
                {
                    console.log(data);
                }
            });
        }

        setInterval(function(){
            notif();
            ubahStatus();
        }, 2000);
    });
</script>


</body>

</html>