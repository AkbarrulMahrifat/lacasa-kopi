<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/12/2019
 * Time: 11:29 PM
 */ ?>


<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-uppercase">Tagihan</h2>
                    <hr>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <h5 class="mb-0">Daftar Tagihan</h5>
                        <a class="btn btn-sm btn-primary text-light ml-auto w-auto" data-toggle="modal" data-target="#tambahTagihan"><i class="fa fa-plus"> Tagihan</i></a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered js-basic-example dataTable" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Tagihan</th>
                                    <th>Nominal</th>
                                    <th>Sisa</th>
                                    <th>Jatuh Tempo</th>
                                    <th>Jenis Tagihan</th>
                                    <th style="width: 130px">Status / Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $no =1;
                                    foreach ($tagihan as $t){
                                ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=$t->nama_tagihan?></td>
                                    <td><?='Rp. '.number_format($t->nominal, '0', ',', '.')?></td>
                                    <td><?='Rp. '.number_format($t->sisa, '0', ',', '.')?></td>
                                    <td><?=$t->tgl_jatuhtempo?></td>
                                    <td><?=$t->jenis_tagihan?></td>
                                    <?php if ($t->status == 0){?>
                                    <td class="text-center bg-warning">
                                        <a class="btn btn-xs btn-rounded btn-primary text-light bayarTagihan" data-toggle="tooltip" data-placement="top" data-original-title="Bayar Tagihan" data-idtagihan="<?=$t->id_tagihan?>" data-namatagihan="<?=$t->nama_tagihan?>" data-nominal="<?=$t->nominal?>" data-sisa="<?=$t->sisa?>" data-tgljatuhtempo="<?=$t->tgl_jatuhtempo?>" data-jenistagihan="<?=$t->jenis_tagihan?>" data-status="<?=$t->status?>"><i class="fa fa-donate"></i></a>
                                        <a class="btn btn-xs btn-rounded btn-success text-light" data-toggle="tooltip" data-placement="top" data-original-title="Lunasi Tagihan" onclick="return confirm('Apakah anda yakin ingin mengubah status data ini menjadi Lunas ?')" href="<?=site_url('Pegawai/lunasTagihan/'.$t->id_tagihan)?>"><i class="fa fa-check"></i></a>
                                        <a class="btn btn-xs btn-rounded btn-info text-light editTagihan" data-toggle="tooltip" data-placement="top" data-original-title="Edit Tagihan" data-idtagihan="<?=$t->id_tagihan?>" data-namatagihan="<?=$t->nama_tagihan?>" data-nominal="<?=$t->nominal?>" data-sisa="<?=$t->sisa?>" data-tgljatuhtempo="<?=$t->tgl_jatuhtempo?>" data-jenistagihan="<?=$t->jenis_tagihan?>" data-status="<?=$t->status?>"><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-xs btn-rounded btn-danger text-light" data-toggle="tooltip" data-placement="top" data-original-title="Hapus Tagihan" onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" href="<?=site_url('Pegawai/hapusTagihan/'.$t->id_tagihan)?>"><i class="fa fa-trash"></i></a>
                                    </td>
                                    <?php }else{ ?>
                                    <td class="text-center bg-success text-white">
                                        Lunas
                                    </td>
                                    <?php } ?>
                                </tr>
                                <?php } ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="tambahTagihan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Tagihan</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pegawai/tambahTagihan')?>" method="post" class="needs-validation" novalidate>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="nama">Nama Tagihan</label>
                                <input type="text" class="form-control" id="nama_tagihan" placeholder="Masukkan Nama Tagihan" name="nama_tagihan" required>
                                <div class="invalid-feedback">
                                    Masukkan Nama Tagihan Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="nominal">Nominal</label>
                                <input type="number" class="form-control" id="nominal" min="0" placeholder="Masukkan Nominal Tagihan" aria-describedby="inputGroupPrepend" name="nominal" required>
                                <div class="invalid-feedback">
                                    Masukkan Nominal Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="jatuh_tempo">Tanggal Jatuh Tempo</label>
                                <input type="date" class="form-control" id="jatuh_tempo" placeholder="Masukkan tanggal jatuh tempo" aria-describedby="inputGroupPrepend" name="tgl_jatuhtempo" required>
                                <div class="invalid-feedback">
                                    Masukkan Tanggal Jatuh Tempo Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="jenis_tagihan">Jenis Tagihan</label>
                                <select name="jenis_tagihan" id="jenis_tagihan" class="form-control show-tick" required>
                                    <option value="">-- Pilih Jenis Tagihan --</option>
                                    <option value="Bulanan">Bulanan</option>
                                    <option value="Tahunan">Tahunan</option>
                                    <option value="Tidak Tentu">Tidak Tentu</option>
                                </select>
                                <div class="invalid-feedback">
                                    Masukkan Tanggal Jatuh Tempo Dengan Benar !
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary">Reset</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editTagihan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Tagihan</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pegawai/editTagihan')?>" method="post" class="needs-validation" novalidate>
                    <div class="modal-body">
                        <div class="row">
                            <input id="editidtagihan" name="id_tagihan" hidden>
                            <input id="editsisa" name="sisa" hidden>
                            <input id="nominallama" name="nominallama" hidden>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="editnamatagihan">Nama Tagihan</label>
                                <input type="text" class="form-control" id="editnamatagihan" placeholder="Masukkan Nama Tagihan" name="nama_tagihan" required>
                                <div class="invalid-feedback">
                                    Masukkan Nama Tagihan Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="editnominal">Nominal</label>
                                <input type="number" class="form-control" id="editnominal" min="0" placeholder="Masukkan Nominal Tagihan" aria-describedby="inputGroupPrepend" name="nominal" required>
                                <div class="invalid-feedback">
                                    Masukkan Nominal Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="edittgljatuhtempo">Tanggal Jatuh Tempo</label>
                                <input type="date" class="form-control" id="edittgljatuhtempo" placeholder="Masukkan tanggal jatuh tempo" aria-describedby="inputGroupPrepend" name="tgl_jatuhtempo" required>
                                <div class="invalid-feedback">
                                    Masukkan Tanggal Jatuh Tempo Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="editjenistagihan">Jenis Tagihan</label>
                                <select name="jenis_tagihan" id="editjenistagihan" class="form-control show-tick" required>
                                    <option value="">-- Pilih Jenis Tagihan --</option>
                                    <option value="Bulanan">Bulanan</option>
                                    <option value="Tahunan">Tahunan</option>
                                    <option value="Tidak Tentu">Tidak Tentu</option>
                                </select>
                                <div class="invalid-feedback">
                                    Masukkan Tanggal Jatuh Tempo Dengan Benar !
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-secondary" data-dismiss="modal">Batal</a>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="bayarTagihan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Pembayaran Tagihan</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pegawai/bayarTagihan')?>" method="post" class="needs-validation" novalidate>
                    <div class="modal-body">
                        <div class="row">
                            <input id="bayaridtagihan" name="id_tagihan" hidden>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="bayarnamatagihan">Nama Tagihan</label>
                                <input type="text" class="form-control bg-white" id="bayarnamatagihan" placeholder="Masukkan Nama Tagihan" name="nama_tagihan" readonly>
                                <div class="invalid-feedback">
                                    Masukkan Nama Tagihan Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="bayarnominal">Nominal Yang Harus Dibayar</label>
                                <input type="number" class="form-control bg-white" id="bayarnominal" min="0" placeholder="Masukkan Nominal Tagihan" aria-describedby="inputGroupPrepend" name="nominal" readonly>
                                <div class="invalid-feedback">
                                    Masukkan Nominal Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="bayar">Bayar</label>
                                <input type="number" class="form-control" id="bayar" min="1" placeholder="Masukkan Nominal Pembayaran Tagihan" aria-describedby="inputGroupPrepend" name="pembayaran" required>
                                <div class="invalid-feedback">
                                    Masukkan Pembayaran Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="sisa">Sisa</label>
                                <input type="number" class="form-control bg-white" id="sisa" placeholder="Sisa Pembayaran Tagihan" aria-describedby="inputGroupPrepend" name="sisa" readonly>
                                <div class="invalid-feedback">
                                    Masukkan Nominal Pembayaran Terlebih Dahulu !
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-secondary" data-dismiss="modal">Batal</a>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('.editTagihan').on('click', function () {
                $('#editidtagihan').val($(this).data('idtagihan'));
                $('#editnamatagihan').val($(this).data('namatagihan'));
                $('#editnominal').val($(this).data('nominal'));
                $('#nominallama').val($(this).data('nominal'));
                $('#editsisa').val($(this).data('sisa'));
                $('#edittgljatuhtempo').val($(this).data('tgljatuhtempo'));
                $('#editjenistagihan').val($(this).data('jenistagihan'));
                $('#editstatus').val($(this).data('status')).change();
                $('#editTagihan').modal('show');
            });

            $('.bayarTagihan').on('click', function () {
                $('#bayaridtagihan').val($(this).data('idtagihan'));
                $('#bayarnamatagihan').val($(this).data('namatagihan'));
                $('#bayarnominal').val($(this).data('sisa'));
                $('#bayar').attr("max", $(this).data('sisa'));
                $('#bayarTagihan').modal('show');
            });

            $('#bayar').on('input', function() {
                var bayar = $('#bayar').val();
                var nominal = parseInt($('#bayarnominal').val());

                var sisa = bayar - nominal;
                $('#sisa').attr("value", sisa);
            });
        });
    </script>