<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/12/2019
 * Time: 11:29 PM
 */ ?>


<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-uppercase">Pengeluaran</h2>
                    <hr>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <h5 class="mb-0">Daftar Pengeluaran</h5>
                        <a class="btn btn-sm btn-primary text-light ml-auto w-auto" data-toggle="modal" data-target="#tambahPengeluaran"><i class="fa fa-plus"> Pengeluaran</i></a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered js-basic-example dataTable" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Pengeluaran</th>
                                    <th>Tanggal</th>
                                    <th>Nominal</th>
                                    <th>Nama Pegawai</th>
                                    <th>Jenis Transaksi</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $no =1;
                                    foreach ($pengeluaran as $t){
                                ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=$t->nama_transkeluar?></td>
                                    <td><?=$t->tgl_transkeluar?></td>
                                    <td><?='Rp. '.number_format($t->total, '0', ',', '.')?></td>
                                    <td><?=$t->nama?></td>
                                    <td><?=$t->jenis_transkeluar?></td>
                                    <td class="text-center">
                                        <a class="btn btn-xs btn-rounded btn-info text-light editPengeluaran" data-toggle="tooltip" data-placement="top" data-original-title="Edit Pengeluaran" data-idpengeluaran="<?=$t->id_transkeluar?>" data-tglpengeluaran="<?=$t->tgl_transkeluar?>" data-namapengeluaran="<?=$t->nama_transkeluar?>" data-totalpengeluaran="<?=$t->total?>" data-jenispengeluaran="<?=$t->jenis_transkeluar?>" ><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-xs btn-rounded btn-danger text-light" data-toggle="tooltip" data-placement="top" data-original-title="Hapus Pengeluaran" onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" href="<?=site_url('Pegawai/hapusPengeluaran/'.$t->id_transkeluar)?>"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="tambahPengeluaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Pengeluaran</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pegawai/tambahPengeluaran')?>" method="post" class="needs-validation" novalidate>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="nama">Nama Pengeluaran</label>
                                <input type="text" class="form-control" id="nama_pengeluaran" placeholder="Masukkan Nama Pengeluaran" name="nama_pengeluaran" required>
                                <div class="invalid-feedback">
                                    Masukkan Nama Pengeluaran Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="nominal">Nominal</label>
                                <input type="number" class="form-control" id="nominal" min="0" placeholder="Masukkan Nominal Pengeluaran" aria-describedby="inputGroupPrepend" name="nominal" required>
                                <div class="invalid-feedback">
                                    Masukkan Nominal Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="jatuh_tempo">Tanggal Pengeluaran</label>
                                <input type="date" class="form-control" id="tanggal" placeholder="Masukkan tanggal pengeluaran" aria-describedby="inputGroupPrepend" name="tanggal" required>
                                <div class="invalid-feedback">
                                    Masukkan Tanggal Pengeluaran Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="jenis_pengeluaran">Jenis Pengeluaran</label>
                                <select name="jenis_pengeluaran" id="jenis_pengeluaran" class="form-control show-tick" required>
                                    <option value="">-- Pilih Jenis Pengeluaran --</option>
                                    <option value="Belanja">Belanja</option>
                                    <option value="Tagihan">Tagihan</option>
                                    <option value="Lain-lain">Lain-lain</option>
                                </select>
                                <div class="invalid-feedback">
                                    Pilih Jenis Pengeluaran Dengan Benar !
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary">Reset</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editPengeluaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Tagihan</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pegawai/editPengeluaran')?>" method="post" class="needs-validation" novalidate>
                    <div class="modal-body">
                        <div class="row">
                            <input id="idpengeluaran" name="id_pengeluaran">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="nama">Nama Pengeluaran</label>
                                <input type="text" class="form-control" id="namapengeluaran" placeholder="Masukkan Nama Pengeluaran" name="nama_pengeluaran" required>
                                <div class="invalid-feedback">
                                    Masukkan Nama Pengeluaran Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="nominalpengeluaran">Nominal</label>
                                <input type="number" class="form-control" id="nominalpengeluaran" min="0" placeholder="Masukkan Nominal Pengeluaran" aria-describedby="inputGroupPrepend" name="nominal" required>
                                <div class="invalid-feedback">
                                    Masukkan Nominal Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="tanggalpengeluaran">Tanggal Pengeluaran</label>
                                <input type="date" class="form-control" id="tanggalpengeluaran" placeholder="Masukkan tanggal pengeluaran" aria-describedby="inputGroupPrepend" name="tanggal" required>
                                <div class="invalid-feedback">
                                    Masukkan Tanggal Pengeluaran Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="jenispengeluaran">Jenis Pengeluaran</label>
                                <select name="jenis_pengeluaran" id="jenispengeluaran" class="form-control show-tick" required>
                                    <option value="">-- Pilih Jenis Pengeluaran --</option>
                                    <option value="Belanja">Belanja</option>
                                    <option value="Tagihan">Tagihan</option>
                                    <option value="Lain-lain">Lain-lain</option>
                                </select>
                                <div class="invalid-feedback">
                                    Pilih Jenis Pengeluaran Dengan Benar !
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-secondary" data-dismiss="modal">Batal</a>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('.editPengeluaran').on('click', function () {
                $('#idpengeluaran').val($(this).data('idpengeluaran'));
                $('#namapengeluaran').val($(this).data('namapengeluaran'));
                $('#nominalpengeluaran').val($(this).data('totalpengeluaran'));
                $('#tanggalpengeluaran').val($(this).data('tglpengeluaran'));
                $('#jenispengeluaran').val($(this).data('jenispengeluaran'));
                $('#editPengeluaran').modal('show');
            });

        });
    </script>