<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/12/2019
 * Time: 11:29 PM
 */ ?>


<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-uppercase"><?=$this->uri->segment(2);?></h2>
                    <hr>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- sales  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">
                                    Rata-rata Pendapatan
                                    <a href="#" data-toggle="popover" title="Rata-rata Pendapatan Minggu Terakhir" data-content="Rata-rata pendapatan tanggal <?=date('d M Y', strtotime($total_pendapatan[0]['tgl_trans']))?> hingga <?=date('d M Y', strtotime('+6 day',strtotime($total_pendapatan[0]['tgl_trans'])))?> adalah sebesar <?="Rp. ".number_format($rata2_pendapatan['rata2'], '0', '.', '.')?>. Mengalami <?php if ($rata2_pendapatan['status'] == "Naik"){echo "kenaikan";}else{echo "penurunan";}?> sebesar <?=$rata2_pendapatan['prosentase']?>% dari minggu sebelumnya.">
                                        <i class="fa fa-fw fa-question-circle"></i>
                                    </a>
                                </h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1"><?="Rp. ".number_format($rata2_pendapatan['rata2'], '0', '.', '.')?></h1>
                                </div>
                                <br>
                                <?php if ($rata2_pendapatan['status'] == "Naik"){?>
                                <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                                    <span class="icon-circle-small icon-box-xs text-success bg-success-light"><i class="fa fa-fw fa-arrow-up"></i></span><span class="ml-1"><?=$rata2_pendapatan['prosentase']?>%</span>
                                </div>
                                <?php } else{?>
                                <div class="metric-label d-inline-block float-right text-danger font-weight-bold">
                                    <span class="icon-circle-small icon-box-xs text-danger bg-danger-light bg-danger-light "><i class="fa fa-fw fa-arrow-down"></i></span><span class="ml-1"><?=$rata2_pendapatan['prosentase']?>%</span>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end sales  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- new customer  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">Rata-rata Pengeluaran
                                    <a href="#" data-toggle="popover" title="Rata-rata Pengeluaran Minggu Terakhir" data-content="Rata-rata pengeluaran tanggal <?=date('d M Y', strtotime($total_pendapatan[0]['tgl_trans']))?> hingga <?=date('d M Y', strtotime('+6 day',strtotime($total_pendapatan[0]['tgl_trans'])))?> adalah sebesar <?="Rp. ".number_format($rata2_pengeluaran['rata2'], '0', '.', '.')?>. Mengalami <?php if ($rata2_pengeluaran['status'] == "Naik"){echo "kenaikan";}else{echo "penurunan";}?> sebesar <?=$rata2_pengeluaran['prosentase']?>% dari minggu sebelumnya.">
                                        <i class="fa fa-fw fa-question-circle"></i>
                                    </a>
                                </h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1"><?="Rp. ".number_format($rata2_pengeluaran['rata2'], '0', '.', '.')?></h1>
                                </div>
                                <br>
                                <?php if ($rata2_pengeluaran['status'] == "Naik"){?>
                                    <div class="metric-label d-inline-block float-right text-danger font-weight-bold">
                                        <span class="icon-circle-small icon-box-xs text-danger bg-danger-light"><i class="fa fa-fw fa-arrow-up"></i></span><span class="ml-1"><?=$rata2_pengeluaran['prosentase']?>%</span>
                                    </div>
                                <?php } else{?>
                                    <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                                        <span class="icon-circle-small icon-box-xs text-success bg-success-light bg-danger-light "><i class="fa fa-fw fa-arrow-down"></i></span><span class="ml-1"><?=$rata2_pengeluaran['prosentase']?>%</span>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end new customer  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- visitor  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">Rata-rata Penjualan
                                    <a href="#" data-toggle="popover" title="Rata-rata Penjualan Minggu Terakhir" data-content="Rata-rata penjualan tanggal <?=date('d M Y', strtotime($total_pendapatan[0]['tgl_trans']))?> hingga <?=date('d M Y', strtotime('+6 day',strtotime($total_pendapatan[0]['tgl_trans'])))?> adalah sebesar <?=$rata2_penjualan['rata2']?> porsi. Mengalami <?php if ($rata2_penjualan['status'] == "Naik"){echo "kenaikan";}else{echo "penurunan";}?> sebesar <?=$rata2_penjualan['prosentase']?>% dari minggu sebelumnya.">
                                        <i class="fa fa-fw fa-question-circle"></i>
                                    </a>
                                </h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1"><?=number_format($rata2_penjualan['rata2'], '0', '.', '.')?> porsi</h1>
                                </div>
                                <br>
                                <?php if ($rata2_penjualan['status'] == "Naik"){?>
                                    <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                                        <span class="icon-circle-small icon-box-xs text-success bg-success-light"><i class="fa fa-fw fa-arrow-up"></i></span><span class="ml-1"><?=$rata2_penjualan['prosentase']?>%</span>
                                    </div>
                                <?php } else{?>
                                    <div class="metric-label d-inline-block float-right text-danger font-weight-bold">
                                        <span class="icon-circle-small icon-box-xs text-danger bg-danger-light bg-danger-light "><i class="fa fa-fw fa-arrow-down"></i></span><span class="ml-1"><?=$rata2_penjualan['prosentase']?>%</span>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end visitor  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- total orders  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">Total Penjualan
                                    <a href="#" data-toggle="popover" title="Total Penjualan Minggu Terakhir" data-content="Total penjualan tanggal <?=date('d M Y', strtotime($total_pendapatan[0]['tgl_trans']))?> hingga <?=date('d M Y', strtotime('+6 day',strtotime($total_pendapatan[0]['tgl_trans'])))?> adalah sebesar <?=$pesanan['jumlah']?>. Mengalami <?php if ($pesanan['status'] == "Naik"){echo "kenaikan";}else{echo "penurunan";}?> sebesar <?=$pesanan['prosentase']?>% dari minggu sebelumnya.">
                                        <i class="fa fa-fw fa-question-circle"></i>
                                    </a>
                                </h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1"><?=$pesanan['jumlah']?> porsi</h1>
                                </div>
                                <br>
                                <?php if ($pesanan['status'] == "Naik"){?>
                                    <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                                        <span class="icon-circle-small icon-box-xs text-success bg-success-light"><i class="fa fa-fw fa-arrow-up"></i></span><span class="ml-1"><?=$pesanan['prosentase']?>%</span>
                                    </div>
                                <?php } else{?>
                                    <div class="metric-label d-inline-block float-right text-danger font-weight-bold">
                                        <span class="icon-circle-small icon-box-xs text-danger bg-danger-light bg-danger-light "><i class="fa fa-fw fa-arrow-down"></i></span><span class="ml-1"><?=$pesanan['prosentase']?>%</span>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end total orders  -->
                    <!-- ============================================================== -->
                </div>


                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Grafik Penjualan dan Peramalan Per Minggu</h5>
                        <div class="card-body">
                            <div class="col-12 text-center">
                                <select id="bahanbaku" class="form-control">
                                    <?php foreach ($bahanbaku as $bb){ ?>
                                        <option value="<?=$bb->id_bahanbaku?>"><?=$bb->nama_bahanbaku?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div id="chartLine">
                                <canvas id="lineChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

<!--    <script src="--><?//=base_url()?><!--assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>-->
<!--    <script src="--><?//=base_url()?><!--assets/assets/libs/js/main-js.js"></script>-->
    <script src="<?=base_url()?>assets/assets/vendor/charts/charts-bundle/Chart.bundle.js"></script>
    <script src="<?=base_url()?>assets/assets/vendor/charts/charts-bundle/chartjs.js"></script>

    <script>
        jQuery(document).ready(function($) {
            "use strict";

            var idbb = $('#bahanbaku').val();
            $.ajax({
                url: "<?=site_url('Pemilik/getGrafikPenjualanPeramalan/')?>"+idbb,
                type:'GET',
                // dataType:'json',
                success: function(a){
                    var data = JSON.parse(a);
                    $('#lineChart').remove();
                    $('#chartLine').append('<canvas id="lineChart"></canvas>');
                    //line chart
                    let ctx = document.getElementById( "lineChart" );
                    ctx.height = 150;
                    var aktual = data.aktual;
                    var peramalan = data.peramalan;
                    var label = data.bln;
                    var myChart = new Chart( ctx, {
                        type: 'bar',
                        data: {
                            labels: label,
                            datasets: [
                                {
                                    label: "Data Aktual",
                                    borderColor: "rgba(0,0,0,.09)",
                                    borderWidth: "1",
                                    backgroundColor: "rgba(0,0,0,.07)",
                                    data: aktual
                                },
                                {
                                    label: "Hasil Peramalan",
                                    borderColor: "rgba(0, 123, 255, 0.9)",
                                    borderWidth: "1",
                                    backgroundColor: "rgba(0, 123, 255, 0.5)",
                                    pointHighlightStroke: "rgba(26,179,148,1)",
                                    data: peramalan
                                }
                            ]
                        },
                        options: {
                            responsive: true,
                            tooltips: {
                                mode: 'index',
                                intersect: false
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            }

                        }
                    } );
                }
            });


            $('#bahanbaku').on('change',function(){
                var idbb = $('#bahanbaku').val();
                $.ajax({
                    url: "<?=site_url('Pemilik/getGrafikPenjualanPeramalan/')?>"+idbb,
                    type: "GET",
                    // dataType: "json",
                    success: function(a){
                        var data = JSON.parse(a);
                        $('#lineChart').remove();
                        $('#chartLine').append('<canvas id="lineChart"></canvas>');
                        //line chart
                        let ctx = document.getElementById( "lineChart" );
                        ctx.height = 150;
                        var aktual = data.aktual;
                        var peramalan = data.peramalan;
                        var label = data.periode;
                        var myChart = new Chart( ctx, {
                            type: 'bar',
                            data: {
                                labels: label,
                                datasets: [
                                    {
                                        label: "Data Aktual",
                                        borderColor: "rgba(0,0,0,.09)",
                                        borderWidth: "1",
                                        backgroundColor: "rgba(0,0,0,.07)",
                                        data: aktual
                                    },
                                    {
                                        label: "Hasil Peramalan",
                                        borderColor: "rgba(0, 123, 255, 0.9)",
                                        borderWidth: "1",
                                        backgroundColor: "rgba(0, 123, 255, 0.5)",
                                        pointHighlightStroke: "rgba(26,179,148,1)",
                                        data: peramalan
                                    }
                                ]
                            },
                            options: {
                                responsive: true,
                                tooltips: {
                                    mode: 'index',
                                    intersect: false
                                },
                                hover: {
                                    mode: 'nearest',
                                    intersect: true
                                }

                            }
                        } );
                    },
                    error: function(data){
                        alert("Terjadi kesalahan pada sistem");
                    }
                });
            })

        });

        // jQuery(document).ready(function($){
        //     function dynamicColors() {
        //         var r = Math.floor(Math.random() * 255);
        //         var g = Math.floor(Math.random() * 255);
        //         var b = Math.floor(Math.random() * 255);
        //         return "rgba(" + r + "," + g + "," + b + ", 0.5)";
        //     }
        //
        //     function poolColors(a) {
        //         var pool = [];
        //         for(var i = 0; i < a; i++) {
        //             pool.push(dynamicColors());
        //         }
        //         return pool;
        //     }
        //
        //     var id = $('#mitra').val();
        //     $.ajax({
        //         url:'/getStokBeranda/'+id,
        //         type:'GET',
        //         dataType:'json',
        //         success: function(data){
        //             $('#pieChart').remove();
        //             $('#chartPie').append('<canvas id="pieChart"></canvas>');
        //             let ct = document.getElementById( "pieChart" );
        //             var arr = [];
        //             var label = [];
        //             data.forEach(function(datum){
        //                 arr.push(datum.jumlah);
        //                 label.push(datum.nama_bahanbaku);
        //             });
        //             var chart = new Chart( ct, {
        //                 type: 'pie',
        //                 data: {
        //                     datasets: [ {
        //                         data: arr,
        //                         backgroundColor: poolColors(arr.length),
        //                     } ],
        //                     labels: label
        //                 },
        //                 options: {
        //                     responsive: true
        //                 }
        //             } );
        //         }
        //     })
        //
        //     $('#mitra').on('change',function(){
        //         var id = $('#mitra').val();
        //         $.ajax({
        //             url:'/getStokBeranda/'+id,
        //             type:'GET',
        //             dataType:'json',
        //             success: function(data){
        //                 $('#pieChart').remove();
        //                 $('#chartPie').append('<canvas id="pieChart"></canvas>');
        //                 let ct = document.getElementById( "pieChart" );
        //                 var arr = [];
        //                 var label = [];
        //                 data.forEach(function(datum){
        //                     arr.push(datum.jumlah);
        //                     label.push(datum.nama_bahanbaku);
        //                 });
        //                 var chart = new Chart( ct, {
        //                     type: 'pie',
        //                     data: {
        //                         datasets: [ {
        //                             data: arr,
        //                             backgroundColor: poolColors(arr.length),
        //                         } ],
        //                         labels: label
        //                     },
        //                     options: {
        //                         responsive: true
        //                     }
        //                 } );
        //             }
        //         })
        //     })
        // });
    </script>