<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/12/2019
 * Time: 11:29 PM
 */ ?>


<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-uppercase">Rekap Transaksi</h2>
                    <hr>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <h5 class="mb-0">Transaksi Keluar</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered js-basic-example dataTable" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Nama Transaksi</th>
                                    <th>Pemasukan</th>
                                    <th>Petugas</th>
                                    <th>Jenis Pengeluaran</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $no =1;
                                    foreach ($transaksi as $t){
                                ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=$t['tgl_transkeluar']?></td>
                                    <td><?=$t['nama_transkeluar']?></td>
                                    <td><?=$t['total']?></td>
                                    <td><?=$t['nama']?></td>
                                    <td><?=$t['jenis_transkeluar']?></td>
                                </tr>
                                <?php } ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="detailPenjualan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width: 1500px">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Pesanan</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="" method="post" class="needs-validation" novalidate>
                    <div class="modal-body" style="padding-bottom: 0px;padding-left: 0px;padding-right: 0px;padding-top: 0px;">
                        <div class="row">
                            <div class="offset-xl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div id="detailTrans"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-primary" data-dismiss="modal">Oke</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editTransaksi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Transaksi</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pegawai/ubahTransaksi')?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <div class="table-responsive">
                                    <table id="" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Menu</th>
                                            <th>Qty</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tabelDetailTrans">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <input id="editidtrans" name="id_trans" type="hidden">
                            <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="edittgltrans">Tanggal Transaksi</label>
                                <input type="date" class="form-control" id="edittgltrans" placeholder="Masukkan tanggal transaksi" aria-describedby="inputGroupPrepend" name="tgl_trans" required>
                                <div class="invalid-feedback">
                                    Masukkan Tanggal Dengan Benar !
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="editnomeja">Nomor Meja</label>
                                <input type="number" min="1" class="form-control" id="editnomeja" placeholder="Masukkan nomor meja" aria-describedby="inputGroupPrepend" name="nomeja" value="" required>
                                <div class="invalid-feedback">
                                    Masukkan Nomor Meja Dengan Benar !
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="edittotal">Total</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp.</span>
                                    </div>
                                    <input type="number" min="1" class="form-control bg-white" id="edittotal" placeholder="Masukkan total belanja" aria-describedby="inputGroupPrepend" name="total" readonly>
                                </div>
                                <div class="invalid-feedback">
                                    Masukkan Total Dengan Benar !
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="editbayar">Bayar</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp.</span>
                                    </div>
                                    <input type="number" class="form-control" id="editbayar" placeholder="Nominal bayar" aria-describedby="inputGroupPrepend" name="bayar" value="" required>
                                </div>
                                <div class="invalid-feedback">
                                    Masukkan Nominal Pembayaran Dengan Benar !
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="editkembali">Kembalian</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp.</span>
                                    </div>
                                    <input type="number" class="form-control bg-white editkembali" id="editkembali" min="1" placeholder="Nominal kembalian" aria-describedby="inputGroupPrepend" name="kembali" value="" readonly>
                                    <input type="hidden" class="form-control bg-white" id="editkembalilama" min="1" placeholder="Nominal kembalian" aria-describedby="inputGroupPrepend" name="kembalianlama" value="" readonly>
                                </div>
                                <div class="invalid-feedback">
                                    Masukkan Nominal Kembalian Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="editstatus">Status Pembayaran</label>
                                <select class="form-control" id="editstatus" aria-describedby="inputGroupPrepend" name="status" required>
                                    <option value="">Pilih Status</option>
                                    <option value="1">Belum Bayar</option>
                                    <option value="2">Sudah Bayar</option>
                                </select>
                                <div class="invalid-feedback">
                                    Pilih Status Pembayaran Dengan Benar !
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script src="<?=base_url()?>assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.detailPenjualan').on('click', function () {
                var id = $(this).data('idtrans');
                $.get("<?php echo site_url('Pegawai/getDetailTrans/'); ?>"+id,function (msg) {
                        $('#detailTrans').html(msg);
                });
                $('#detailPenjualan').modal('show');
            });

            $('.editTrans').on('click', function () {
                var id = $(this).data('idtrans');
                $.get("<?php echo site_url('Pegawai/getDetailTransKasir/'); ?>"+id,function (msg) {
                    $('#tabelDetailTrans').html(msg);
                });

                $('#editidtrans').val($(this).data('idtrans'));
                $('#edittgltrans').val($(this).data('tgltrans'));
                $('#editnomeja').val($(this).data('nomeja'));
                $('#edittotal').val($(this).data('total'));
                $('#editbayar').val($(this).data('bayar'));
                $('#editstatus').val($(this).data('status')).change();
                $('#editkembalilama').val($(this).data('kembali'));

                $('#editbayar').on('input', function() {
                    var bayar = $('#editbayar').val();
                    var total = parseInt($('#edittotal').val());

                    var kembalian = bayar - total;
                    $('#editkembali').attr("value", kembalian);

                });
                $('#editTransaksi').modal('show');
            });
        });
    </script>