<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/13/2019
 * Time: 2:07 PM
 */ ?>

<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title text-uppercase">Kasir</h2>
                        <hr>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 m-b-20">
                    <h3 class="section-title">Daftar Menu :</h3>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 text-right m-b-20">
                    <a href="#" class="btn btn-success keranjang" data-toggle="tooltip" data-placement="top" data-original-title="Keranjang"><i class="fa fa-shopping-cart"></i> Keranjang Belanja</a>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="row">
                        <?php foreach($menu as $m){ ?>
                        <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="product-thumbnail">
                                <div class="product-img-head">
                                    <div class="product-img">
                                        <img src="<?=base_url()?>/assets/FotoMenu/<?=$m['gambar']?>" alt="" class="img-fluid" style="width: 500px; height: 150px">
                                    </div>
                                    <?php if ($m['stok'] == 0){?>
                                        <div class="ribbons bg-danger"></div>
                                        <div class="ribbons-text">Habis</div>
                                    <?php } ?>
                                </div>
                                <div class="product-content">
                                    <div class="product-content-head">
                                        <h3 class="product-title" style="min-height: 50px"><?=$m['nama_menu']?></h3>
                                        <div class="product-price">Rp. <?=number_format($m['harga'], '2', ',', '.')?></div>
                                        <br>
                                        <p class="product-price">Stok : <?=$m['stok']?> porsi</p>
                                    </div>
                                    <div class="product-btn">
                                        <a class="btn btn-primary text-light tambahKeranjang" data-toggle="tooltip" data-placement="top" data-original-title="Tambah Ke Keranjang" data-idmenu="<?=$m['id_menu']?>" data-namamenu="<?=$m['nama_menu']?>" data-hargamenu="<?=$m['harga']?>" data-stokmenu="<?=$m['stok']?>"><i class="fa fa-shopping-cart"></i></a>
                                        <a class="btn btn-outline-light detail" data-toggle="tooltip" data-placement="top" data-original-title="Detail"  data-idmenu="<?=$m['id_menu']?>" data-namamenu="<?=$m['nama_menu']?>" data-hargamenu="Rp. <?=number_format($m['harga'], '2', ',', '.')?>" data-stokmenu="<?=$m['stok'].' porsi'?>" data-gambar="<?=base_url()?>/assets/FotoMenu/<?=$m['gambar']?>"><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>

                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-header d-flex">
                                    <h5 class="mb-0">Daftar Transaksi Hari Ini</h5>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="" class="table table-striped table-bordered js-basic-example dataTable" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Tanggal Transaksi</th>
                                                <th>No. Meja</th>
                                                <th>Status</th>
                                                <th>Kasir</th>
                                                <th>Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $no =1;
                                            foreach ($transaksi as $t){
                                                ?>
                                                <tr>
                                                    <td><?=$no++?></td>
                                                    <td><?=date('d-m-Y', strtotime($t->tgl_trans))?></td>
                                                    <td><?=$t->nomeja?></td>
                                                    <td>
                                                        <?php
                                                        if ($t->status == 2){
                                                            echo "<span class='badge badge-success'>Sudah Bayar</span>";
                                                        }elseif ($t->status == 1){
                                                            echo "<span class='badge badge-warning'>Belum Bayar</span>";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><?=$t->nama?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-xs btn-rounded btn-info text-light detailTrans" data-toggle="tooltip" data-placement="top" data-original-title="Detail Transaksi" data-idtransaksi="<?=$t->id_trans?>"><i class="fa fa-search"></i></a>
                                                        <a class="btn btn-xs btn-rounded btn-primary text-light editTrans" data-toggle="tooltip" data-placement="top" data-original-title="Edit Transaksi" data-idtrans="<?=$t->id_trans?>" data-tgltrans="<?=$t->tgl_trans?>" data-nomeja="<?=$t->nomeja?>" data-total="<?=$t->total?>" data-bayar="<?=$t->bayar?>" data-kembali="<?=$t->kembali?>" data-status="<?=$t->status?>"><i class="fa fa-edit"></i></a>
                                                        <a class="btn btn-xs btn-rounded btn-danger text-light" data-toggle="tooltip" data-placement="top" data-original-title="Hapus Transaksi" onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" href="<?=site_url('Pegawai/hapusTransaksi/'.$t->id_trans)?>" ><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>


<!--        Modal       -->
        <div class="modal fade" id="keranjang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Daftar Keranjang</h5>
                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </a>
                    </div>
                    <form action="<?=site_url('Pegawai/tambahTransaksi')?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <div class="table-responsive">
                                    <table id="" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Menu</th>
                                            <th>Qty</th>
                                            <th>Status</th>
                                            <th style="width: 90px">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no =1;
                                        foreach ($keranjang as $k){
                                            ?>
                                            <tr>
                                                <td><?=$no++?></td>
                                                <td>
                                                    <?=$k->nama_menu?>
                                                    <input type="hidden" name="menu_id[]" value="<?=$k->id_menu?>">
                                                </td>
                                                <td>
                                                    <?=$k->qty?>
                                                    <input type="hidden" name="qty[]" value="<?=$k->qty?>">
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($k->status == 1){
                                                        echo "Pesanan Diproses";
                                                    }
                                                    if ($k->status == 2){
                                                        echo "Pesanan Selesai";
                                                    }
                                                    ?>
                                                    <input type="hidden" name="statusPesanan[]" value="<?=$k->status?>">
                                                </td>
                                                <td class="text-center">
                                                    <a class="btn btn-xs btn-rounded btn-danger text-light" data-toggle="tooltip" data-placement="top" data-original-title="Hapus Menu" onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" href="<?=site_url('Pegawai/hapusKeranjang/'.$k->id_detailtrans)?>"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>

                                    </table>
                                </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="tgl_trans">Tanggal Transaksi</label>
                                    <input type="date" class="form-control" id="tgl_trans" placeholder="Masukkan tanggal transaksi" aria-describedby="inputGroupPrepend" name="tgl_trans" value="<?=date('Y-m-d')?>" required>
                                    <div class="invalid-feedback">
                                        Masukkan Tanggal Dengan Benar !
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="nomeja">Nomor Meja</label>
                                    <input type="number" min="1" class="form-control" id="nomeja" placeholder="Masukkan nomor meja" aria-describedby="inputGroupPrepend" name="nomeja" value="" required>
                                    <div class="invalid-feedback">
                                        Masukkan Nomor Meja Dengan Benar !
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="total">Total</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Rp.</span>
                                        </div>
                                        <input type="number" min="1" class="form-control bg-white" id="total" placeholder="Masukkan total belanja" aria-describedby="inputGroupPrepend" name="total" value="<?=$total?>" readonly>
                                    </div>
                                    <div class="invalid-feedback">
                                        Masukkan Total Dengan Benar !
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="bayar">Bayar</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Rp.</span>
                                        </div>
                                        <input type="number" class="form-control" id="bayar" placeholder="Nominal bayar" aria-describedby="inputGroupPrepend" name="bayar" value="" required>
                                    </div>
                                    <div class="invalid-feedback">
                                        Masukkan Nominal Pembayaran Dengan Benar !
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="kembali">Kembalian</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Rp.</span>
                                        </div>
                                        <input type="number" class="form-control bg-white" id="kembali" min="1" placeholder="Nominal kembalian" aria-describedby="inputGroupPrepend" name="kembali" value="" readonly>
                                    </div>
                                    <div class="invalid-feedback">
                                        Masukkan Nominal Kembalian Dengan Benar !
                                    </div>
                                </div>

                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="status">Status Pembayaran</label>
                                    <select class="form-control" id="status" aria-describedby="inputGroupPrepend" name="status" required>
                                        <option value="">Pilih Status</option>
                                        <option value="1">Belum Bayar</option>
                                        <option value="2">Sudah Bayar</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Pilih Status Pembayaran Dengan Benar !
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="modal fade" id="tambahKeranjang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Ke Keranjang</h5>
                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </a>
                    </div>
                    <form action="<?=site_url('Pegawai/tambahKeranjang')?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                        <input type="hidden" name="id_menu" id="id_menu">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="nama_menu">Nama Menu</label>
                                    <input type="text" class="form-control bg-white" id="nama_menu" placeholder="Masukkan nama menu" aria-describedby="inputGroupPrepend" name="nama_menu" readonly>
                                    <div class="invalid-feedback">
                                        Masukkan Nama Dengan Benar !
                                    </div>
                                </div>

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="harga_menu">Harga Menu</label>
                                    <input type="number" class="form-control bg-white" min="1" id="harga_menu" placeholder="Masukkan harga menu" aria-describedby="inputGroupPrepend" name="harga_menu" readonly>
                                    <div class="invalid-feedback">
                                        Masukkan Harga Dengan Benar !
                                    </div>
                                </div>

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="stok_menu">Stok</label>
                                    <input type="number" class="form-control bg-white" min="1" id="stok_menu" placeholder="Masukkan harga menu" aria-describedby="inputGroupPrepend" name="stok_menu" readonly>
                                    <div class="invalid-feedback">
                                        Masukkan Harga Dengan Benar !
                                    </div>
                                </div>

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="jumlah_menu">Jumlah Porsi</label>
                                    <input type="number" class="form-control bg-white" min="1" id="jumlah_menu" placeholder="Masukkan jumlah pembelian menu" aria-describedby="inputGroupPrepend" name="jumlah_menu" required>
                                    <div class="invalid-feedback">
                                        Masukkan Jumlah Minimal 1 dan Maksimal <span id="max"></span> !
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" id="submitKeranjang">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Detail Menu</h5>
                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </a>
                    </div>
                    <div class="modal-body">
                        <div class="card">
                            <img id="gambarmenu" class="card-img-top img-fluid" src="" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title">Nama Menu : </h4>
                                <p class="card-text" id="namamenu"></p>
                                <h4 class="card-title">Harga Menu : </h4>
                                <p class="card-text" id="hargamenu"></p>
                                <h4 class="card-title">Stok Menu : </h4>
                                <p class="card-text" id="stokmenu"></p>
                                <h4 class="card-title">Resep : </h4>
                                <div id="resepmenu" class="card-text"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="editTransaksi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Transaksi</h5>
                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </a>
                    </div>
                    <form action="<?=site_url('Pegawai/ubahTransaksi')?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <div class="table-responsive">
                                        <table id="" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama Menu</th>
                                                <th>Qty</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody id="tabelDetailTrans">

                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <input id="editidtrans" name="id_trans" type="hidden">
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="edittgltrans">Tanggal Transaksi</label>
                                    <input type="date" class="form-control" id="edittgltrans" placeholder="Masukkan tanggal transaksi" aria-describedby="inputGroupPrepend" name="tgl_trans" required>
                                    <div class="invalid-feedback">
                                        Masukkan Tanggal Dengan Benar !
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="editnomeja">Nomor Meja</label>
                                    <input type="number" min="1" class="form-control" id="editnomeja" placeholder="Masukkan nomor meja" aria-describedby="inputGroupPrepend" name="nomeja" value="" required>
                                    <div class="invalid-feedback">
                                        Masukkan Nomor Meja Dengan Benar !
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="edittotal">Total</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Rp.</span>
                                        </div>
                                        <input type="number" min="1" class="form-control bg-white" id="edittotal" placeholder="Masukkan total belanja" aria-describedby="inputGroupPrepend" name="total" readonly>
                                    </div>
                                    <div class="invalid-feedback">
                                        Masukkan Total Dengan Benar !
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="editbayar">Bayar</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Rp.</span>
                                        </div>
                                        <input type="number" class="form-control" id="editbayar" placeholder="Nominal bayar" aria-describedby="inputGroupPrepend" name="bayar" value="" required>
                                    </div>
                                    <div class="invalid-feedback">
                                        Masukkan Nominal Pembayaran Dengan Benar !
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="editkembali">Kembalian</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Rp.</span>
                                        </div>
                                        <input type="number" class="form-control bg-white editkembali" id="editkembali" min="1" placeholder="Nominal kembalian" aria-describedby="inputGroupPrepend" name="kembali" value="" readonly>
                                        <input type="hidden" class="form-control bg-white" id="editkembalilama" min="1" placeholder="Nominal kembalian" aria-describedby="inputGroupPrepend" name="kembalianlama" value="" readonly>
                                    </div>
                                    <div class="invalid-feedback">
                                        Masukkan Nominal Kembalian Dengan Benar !
                                    </div>
                                </div>

                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="editstatus">Status Pembayaran</label>
                                    <select class="form-control" id="editstatus" aria-describedby="inputGroupPrepend" name="status" required>
                                        <option value="">Pilih Status</option>
                                        <option value="1">Belum Bayar</option>
                                        <option value="2">Sudah Bayar</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Pilih Status Pembayaran Dengan Benar !
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="modal fade" id="detailTrans" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Detail Pesanan</h5>
                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </a>
                    </div>
                    <form action="" method="post" class="needs-validation" novalidate>
                        <div class="modal-body" style="padding-bottom: 0px;padding-left: 0px;padding-right: 0px;padding-top: 0px;">
                            <div class="row">
                                <div class="offset-xl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div id="showDetailTrans"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-primary" data-dismiss="modal">Oke</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <script src="<?=base_url()?>assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
        <script>
            $(document).ready(function() {
                $('.keranjang').on('click', function () {
                    $('#keranjang').modal('show');
                });

                $('.tambahKeranjang').on('click', function () {
                    $('#id_menu').val($(this).data('idmenu'));
                    $('#nama_menu').val($(this).data('namamenu'));
                    $('#harga_menu').val($(this).data('hargamenu'));
                    $('#stok_menu').val($(this).data('stokmenu'));
                    document.getElementById('jumlah_menu').max = $(this).data('stokmenu');
                    document.getElementById('max').innerHTML = $(this).data('stokmenu');
                    if ($(this).data('stokmenu') === 0) {
                        $('#submitKeranjang').attr("class", "btn btn-danger");
                        $('#submitKeranjang').attr("disabled", true);
                        document.getElementById('submitKeranjang').innerHTML = "Stok Habis !";
                    }
                    $('#tambahKeranjang').modal('show');
                });

                $('.detail').on('click', function () {
                    var id = $(this).data('idmenu');
                    document.getElementById('namamenu').innerHTML = $(this).data('namamenu');
                    document.getElementById('hargamenu').innerHTML = $(this).data('hargamenu');
                    document.getElementById('stokmenu').innerHTML = $(this).data('stokmenu');
                    document.getElementById('gambarmenu').src = $(this).data('gambar');

                    $.get("<?php echo site_url('Pegawai/getResepMenuPerId/'); ?>"+id,function (msg) {
                        $('#resepmenu').html(msg);
                    });

                    $('#detail').modal('show');
                });

                $('#bayar').on('input', function() {
                    var bayar = $('#bayar').val();
                    var total = parseInt($('#total').val());

                    var kembalian = bayar - total;
                    $('#kembali').attr("value", kembalian);
                    // $('#bayar').attr("min", total);

                });

                $('.detailTrans').on('click', function () {
                    var id = $(this).data('idtransaksi');
                    $.get("<?php echo site_url('Pegawai/getDetailTrans/'); ?>"+id,function (msg) {
                        $('#showDetailTrans').html(msg);
                    });

                    $('#detailTrans').modal('show');
                });

                $('.editTrans').on('click', function () {
                    var id = $(this).data('idtrans');
                    $.get("<?php echo site_url('Pegawai/getDetailTransKasir/'); ?>"+id,function (msg) {
                        $('#tabelDetailTrans').html(msg);
                    });

                    $('#editidtrans').val($(this).data('idtrans'));
                    $('#edittgltrans').val($(this).data('tgltrans'));
                    $('#editnomeja').val($(this).data('nomeja'));
                    $('#edittotal').val($(this).data('total'));
                    $('#editbayar').val($(this).data('bayar'));
                    $('#editstatus').val($(this).data('status')).change();
                    $('#editkembalilama').val($(this).data('kembali'));

                    $('#editbayar').on('input', function() {
                        var bayar = $('#editbayar').val();
                        var total = parseInt($('#edittotal').val());

                        var kembalian = bayar - total;
                        $('#editkembali').attr("value", kembalian);

                    });
                    $('#editTransaksi').modal('show');
                });
            });
        </script>