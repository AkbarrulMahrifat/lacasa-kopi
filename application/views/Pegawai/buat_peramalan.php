<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 6/14/2019
 * Time: 12:54 AM
 */ ?>


<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-uppercase">Peramalan</h2>
                    <hr>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <h5 class="mb-0">Buat Peramalan Penjualan Berdasarkan Jenis Bahanbaku Kopi</h5>
                        <form action="<?=site_url('Pegawai/prosesPeramalan')?>" method="post" class="needs-validation" novalidate>
                    </div>
                    <div class="card-body">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="jenis_kopi">Jenis Kopi</label>
                                <select name="jenis_kopi" id="jenis_kopi" class="form-control show-tick" required>
                                    <option value="">-- Pilih Jenis Kopi --</option>
                                    <?php foreach ($bahanbaku as $bb){?>
                                    <option value="<?=$bb->id_bahanbaku?>"><?=$bb->nama_bahanbaku?></option>
                                    <?php } ?>
                                </select>
                                <div class="invalid-feedback">
                                    Pilih Jenis Kopi Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="periode">Periode Peramalan</label>
                                <select name="periode" id="periode" class="form-control show-tick" required>
                                    <option value="">-- Pilih Periode Peramalan --</option>
                                    <?php
                                    $x = 0;
                                    $now= date('Y-m-d');
                                    $date = "2019-01-01";
                                    $tanggal_akhir = date('Y-m-d', strtotime("+7 day", strtotime($now)));
                                    while ($date <= $tanggal_akhir){
                                        $awal = "2019-01-01";
                                        $date = date("Y-m-d", strtotime("+".$x." week", strtotime($awal)));
                                        $date2 = date("Y-m-d", strtotime("+6 day", strtotime($date)));
                                        $x++;
                                        ?>
                                        <option value="<?=$date?>"><?=$date." - ".$date2?></option>
                                    <?php } ?>
                                </select>
                                <div class="invalid-feedback">
                                    Pilih Jenis Kopi Dengan Benar !
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="reset" class="btn btn-secondary">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

