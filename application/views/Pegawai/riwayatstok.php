<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/12/2019
 * Time: 11:29 PM
 */ ?>


<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-uppercase">Riwayat Stok Bahanbaku</h2>
                    <hr>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <h5 class="mb-0">Riwayat Stok Bahanbaku</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered js-basic-example dataTable" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Bahanbaku</th>
                                    <th>Tanggal Beli</th>
                                    <th>Tanggal Kadaluarsa</th>
                                    <th>Jumlah Beli</th>
                                    <th>Sisa</th>
                                    <th>Status</th>
                                    <th style="width: 90px">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $no =1;
                                    foreach ($riwayatstok as $rs){
                                ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=$rs->nama_bahanbaku?></td>
                                    <td><?=$rs->tgl_beli?></td>
                                    <td><?=$rs->tgl_kadaluarsa?></td>
                                    <td><?=$rs->jumlah_beli." ".$rs->satuan?></td>
                                    <td><?=$rs->sisa." ".$rs->satuan?></td>
                                    <td>
                                        <?php
                                        if ($rs->status == 3){
                                            echo "<span class='badge badge-danger'>Kadaluarsa</span>";
                                        }elseif ($rs->status == 2 && $rs->sisa == 0){
                                            echo "<span class='badge badge-warning'>Habis</span>";
                                        }elseif ($rs->status == 1){
                                            echo "<span class='badge badge-info'>Tersedia</span>";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-rounded btn-info text-light editStok" data-toggle="tooltip" data-placement="top" data-original-title="Edit Stok" data-idstok="<?=$rs->id_stok?>" data-idbahanbaku="<?=$rs->bahanbaku_id?>" data-tglbeli="<?=$rs->tgl_beli?>" data-tglkadaluarsa="<?=$rs->tgl_kadaluarsa?>" data-jumlahbeli="<?=$rs->jumlah_beli?>" data-sisa="<?=$rs->sisa?>" data-status="<?=$rs->status?>"><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-xs btn-rounded btn-danger text-light" data-toggle="tooltip" data-placement="top" data-original-title="Hapus Stok" onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" href="<?=site_url('Pegawai/hapusStok/'.$rs->id_stok)?>" ><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="editStok" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Stok</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pegawai/updateStok')?>" method="post" class="needs-validation" novalidate>
                    <input type="hidden" name="id_stok" id="idstok">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="idbahanbaku">Nama Bahanbaku</label>
                                <select id="idbahanbaku" name="bahanbaku_id" class="form-control" required>
                                    <option value="">--- Pilih Nama Bahanbaku ---</option>
                                    <?php foreach ($bahanbaku as $bb){?>
                                        <option value="<?=$bb->id_bahanbaku?>"><?=$bb->nama_bahanbaku?></option>
                                    <?php } ?>
                                </select>
                                <div class="invalid-feedback">
                                    Pilih Bahanbaku Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="tglbeli">Tanggal Beli (Bulan/Hari/Tahun)</label>
                                <input type="date" class="form-control" id="tglbeli" placeholder="Masukkan tanggal beli" aria-describedby="inputGroupPrepend" name="tgl_beli" required>
                                <div class="invalid-feedback">
                                    Masukkan Tanggal Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="tglkadaluarsa">Tanggal Kadaluarsa (Bulan/Hari/Tahun)</label>
                                <input type="date" class="form-control" id="tglkadaluarsa" placeholder="Masukkan tanggal kadaluarsa" aria-describedby="inputGroupPrepend" name="tgl_kadaluarsa" required>
                                <div class="invalid-feedback">
                                    Masukkan Tanggal Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="jumlahbeli">Jumlah</label>
                                <input type="number" min="0" class="form-control" id="jumlahbeli" placeholder="Masukkan jumlah beli" aria-describedby="inputGroupPrepend" name="jumlah_beli" required>
                                <div class="invalid-feedback">
                                    Masukkan Jumlah Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="sisa">Sisa Stok</label>
                                <input type="number" min="0" class="form-control" id="sisa" placeholder="Masukkan sisa stok" aria-describedby="inputGroupPrepend" name="sisa" required>
                                <div class="invalid-feedback">
                                    Masukkan Sisa Stok Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="status">Status</label>
                                <select id="status" name="status" class="form-control" required>
                                    <option value="">--- Pilih Status ---</option>
                                    <option value="1">Tersedia</option>
                                    <option value="2">Habis</option>
                                    <option value="3">Kadaluarsa</option>
                                </select>
                                <div class="invalid-feedback">
                                    Pilih Status Dengan Benar !
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-secondary" data-dismiss="modal">Batal</a>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script src="<?=base_url()?>assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.editStok').on('click', function () {
                $('#idstok').val($(this).data('idstok'));
                $('#idbahanbaku').val($(this).data('idbahanbaku')).change();
                $('#tglbeli').val($(this).data('tglbeli'));
                $('#tglkadaluarsa').val($(this).data('tglkadaluarsa'));
                $('#jumlahbeli').val($(this).data('jumlahbeli'));
                $('#sisa').val($(this).data('sisa'));
                $('#status').val($(this).data('status')).change();
                $('#editStok').modal('show');
            });
        });
    </script>