<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/25/2019
 * Time: 6:05 PM
 */ ?>

<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-uppercase">Manajemen Resep</h2>
                    <hr>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <h5 class="mb-0">Daftar Resep</h5>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <select name="daftar_menu" id="daftar_menu" class="form-control show-tick" required>
                                <option value="0">-- Pilih Menu --</option>
                                <?php foreach ($menu as $m){ ?>
                                    <option value="<?=$m->id_menu?>"><?=$m->nama_menu?></option>
                                 <?php } ?>
                            </select>
                            <br>
                            <div id="daftarResep">
                                <table class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Bahanbaku</th>
                                        <th>Takaran</th>
                                        <th style="width: 150px">Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="text-center">
                                           <td colspan="4">Tidak ada data</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="tambahResep" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Resep</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pegawai/tambahResep')?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 input_fields_wrap">
                                <input type="hidden" id="menu_id" name="menu_id">
                                <label for="resep">Pilih Resep</label>
                                <div class="input-group mb-3">
                                    <select type="number" class="form-control" id="resep" aria-describedby="inputGroupPrepend" name="resep[]" required>
                                        <option value="">--- Pilih Bahanbaku ---</option>
                                        <?php foreach ($bahanbaku as $bb){?>
                                            <option value="<?=$bb->id_bahanbaku?>"><?=$bb->nama_bahanbaku?></option>
                                        <?php } ?>
                                    </select>
                                    <input type="number" class="form-control" min="1" id="takaran" placeholder="Masukkan takaran resep" aria-describedby="inputGroupPrepend" name="takaran[]" required>
                                    <a href="#" class="input-group-text input-group-append"><i class="fa fa-window-close" aria-disabled="true"></i></a>
                                    <div class="invalid-feedback">
                                        Masukkan Resep dan Takaran Dengan Benar !
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <button class="add_field_button btn btn-xs btn-success"><i class="fa fa-plus"></i> Tambah Form Resep</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="reset">Reset</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editResep" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Resep</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <form action="<?=site_url('Pegawai/updateResep')?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                    <input type="hidden" name="id_resep" id="id_resep">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="id_bahanbaku">Nama Bahanbaku</label>
                                <select class="form-control" id="id_bahanbaku" aria-describedby="inputGroupPrepend" name="bahanbaku_id" required>
                                    <option value="">--- Pilih Bahanbaku ---</option>
                                    <?php foreach ($bahanbaku as $bb){?>
                                        <option value="<?=$bb->id_bahanbaku?>"><?=$bb->nama_bahanbaku?></option>
                                    <?php } ?>
                                </select>
                                <div class="invalid-feedback">
                                    Masukkan Nama Dengan Benar !
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="takaran_resep">Takaran</label>
                                <input type="number" class="form-control" min="1" id="takaran_resep" placeholder="Masukkan takaran menu" aria-describedby="inputGroupPrepend" name="takaran" required>
                                <div class="invalid-feedback">
                                    Masukkan Harga Dengan Benar !
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="reset">Reset</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="<?=base_url()?>assets/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#daftar_menu").change(function(){
                $.get("<?php echo site_url('Pegawai/getResep/'); ?>"+$("#daftar_menu").val(),function (msg) {
                    $('#daftarResep').html(msg);

                    $('.tambahResep').on('click', function () {
                        $('#menu_id').val($(this).data('idmenu'));
                        $('#tambahResep').modal('show');
                    });

                    $('.editResep').on('click', function () {
                        $('#id_resep').val($(this).data('idresep'));
                        $('#id_bahanbaku').val($(this).data('idbahanbaku'));
                        $('#takaran_resep').val($(this).data('takaran'));
                        $('#editResep').modal('show');
                    });

                    var max_fields      = 10; //maximum input boxes allowed
                    var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
                    var add_button      = $(".add_field_button"); //Add button ID

                    var x = 1; //initlal text box count
                    $(add_button).click(function(e){ //on add input button click
                        e.preventDefault();
                        if(x < max_fields){ //max input box allowed
                            x++; //text box increment
                            $(wrapper).append('' +
                                '<div class="input-group mb-3">' +
                                '<select type="number" class="form-control" id="resep" aria-describedby="inputGroupPrepend" name="resep[]" required>' +
                                '<option value="">--- Pilih Bahanbaku ---</option>' +
                                '<?php foreach ($bahanbaku as $bb){?>\n' +
                                '<option value="<?=$bb->id_bahanbaku?>"><?=$bb->nama_bahanbaku?></option>\n' +
                                '<?php } ?>' +
                                '</select>' +
                                '<input type="number" class="form-control" min="1" id="takaran" placeholder="Masukkan takaran resep" aria-describedby="inputGroupPrepend" name="takaran[]" required>' +
                                '<a href="#" class="remove_field input-group-text input-group-append"><i class="fa fa-window-close"></i></a>' +
                                '<div class="invalid-feedback">' +
                                'Masukkan Resep dan Takaran Dengan Benar !' +
                                '</div>' +
                                '</div>'
                            ); //add input box
                        }
                    });

                    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                        e.preventDefault(); $(this).parent('div').remove(); x--;
                    })
                });
            });

        });
    </script>
