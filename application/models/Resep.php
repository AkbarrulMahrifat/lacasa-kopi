<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/25/2019
 * Time: 3:00 AM
 */

class Resep extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getResepAll(){
        $this->db->select('*');
        $this->db->from('resep');
        $this->db->join('bahanbaku', 'resep.bahanbaku_id = bahanbaku.id_bahanbaku');
        $this->db->join('menu', 'resep.menu_id = menu.id_menu');
        $this->db->order_by('bahanbaku_id', 'DESC');
        return $this->db->get();
    }

    public function tambahResep($data){
        $insert = $this->db->insert_batch('resep', $data);

        return $insert;
    }

    public function getResepPerMenu($id){
        $this->db->select('*');
        $this->db->from('resep');
        $this->db->join('bahanbaku', 'resep.bahanbaku_id = bahanbaku.id_bahanbaku');
        $this->db->join('menu', 'resep.menu_id = menu.id_menu');
        $this->db->where('menu_id', $id);
        $this->db->order_by('bahanbaku_id', 'DESC');
        return $this->db->get();
    }

    public function getResepPerBahanbaku($id){
        $this->db->select('*');
        $this->db->from('resep');
        $this->db->join('bahanbaku', 'resep.bahanbaku_id = bahanbaku.id_bahanbaku');
        $this->db->where('bahanbaku_id', $id);
        return $this->db->get();
    }

    public function updateResep($data, $id){
        $this->db->where('id_resep', $id);
        $update = $this->db->update('resep', $data);

        return $update;
    }

    public function hapusResep($id){
        $this->db->where('id_resep', $id);
        $this->db->delete('resep');
    }
}