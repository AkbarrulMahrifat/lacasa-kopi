<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 6/6/2019
 * Time: 12:42 AM
 */

class Peramalan extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getPeramalanAll(){
        $this->db->select('*');
        $this->db->from('peramalan');
        $this->db->join('bahanbaku', 'peramalan.bahanbaku_id = bahanbaku.id_bahanbaku');
        $this->db->order_by('periode', 'ASC');
        return $this->db->get();
    }

    public function getPeramalanAwal(){
        $this->db->select('*');
        $this->db->from('peramalan');
        $this->db->order_by('periode', 'ASC');
        return $this->db->get();
    }

    public function tambahPeramalan($data){
        $insert = $this->db->insert('peramalan', $data);

        return $insert;
    }

    public function hapusPeramalan($id){
        $this->db->where('id_peramalan', $id);
        $delete = $this->db->delete('peramalan');
        return $delete;
    }

    public function getPeramalanPerIdBB($id){
        $this->db->select('*');
        $this->db->from('peramalan');
        $this->db->where('bahanbaku_id', $id);
        $this->db->order_by('periode', 'DESC');
        return $this->db->get();
    }
}