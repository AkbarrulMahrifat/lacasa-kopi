<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/27/2019
 * Time: 6:53 AM
 */

class Transaksi extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function tambahTrans($data){
        $this->db->insert('transaksi', $data);
        $id = $this->db->insert_id();

        return $id;
    }

    public function getTransaksiHariIni(){
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->join('user', 'transaksi.user_id = user.id');
        $this->db->where('tgl_trans', date('Y-m-d'));
        $data = $this->db->get();

        return $data;
    }

    public function getTransaksiPerId($id){
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->join('user', 'transaksi.user_id = user.id');
        $this->db->where('id_trans', $id);
        $data = $this->db->get();

        return $data;
    }

    public function getTransaksiAll(){
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->join('user', 'transaksi.user_id = user.id');
        $this->db->order_by('tgl_trans', 'DESC');
        $data = $this->db->get();

        return $data;
    }

    public function getTransaksiPerBahanbaku($id){
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->join('detail_trans', 'transaksi.id_trans = detail_trans.trans_id');
        $this->db->join('menu', 'detail_trans.menu_id = menu.id_menu');
        $this->db->join('resep', 'resep.menu_id = menu.id_menu');
        $this->db->join('bahanbaku', 'resep.bahanbaku_id = bahanbaku.id_bahanbaku');
        $this->db->where('id_bahanbaku', $id);
        $this->db->order_by('tgl_trans', 'ASC');
        $data = $this->db->get();

        return $data;
    }

    public function getTransaksiPerBahanbakuPerOrderLimit($id, $limit, $order){
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->join('detail_trans', 'transaksi.id_trans = detail_trans.trans_id');
        $this->db->join('menu', 'detail_trans.menu_id = menu.id_menu');
        $this->db->join('resep', 'resep.menu_id = menu.id_menu');
        $this->db->join('bahanbaku', 'resep.bahanbaku_id = bahanbaku.id_bahanbaku');
        $this->db->where('id_bahanbaku', $id);
        $this->db->order_by('tgl_trans', $order);
        $this->db->group_by('WEEK(tgl_trans)');
        $this->db->limit($limit);
        $data = $this->db->get();

        return $data;
    }

    public function getTransaksiQtyPerMinggu($id, $date, $date2){
        $this->db->select('*');
        $this->db->select_sum('qty');
        $this->db->from('transaksi');
        $this->db->join('detail_trans', 'transaksi.id_trans = detail_trans.trans_id');
        $this->db->join('menu', 'detail_trans.menu_id = menu.id_menu');
        $this->db->join('resep', 'resep.menu_id = menu.id_menu');
        $this->db->join('bahanbaku', 'resep.bahanbaku_id = bahanbaku.id_bahanbaku');
        $this->db->where('id_bahanbaku', $id);
        $this->db->where('tgl_trans >=', $date);
        $this->db->where('tgl_trans <=', $date2);
        $this->db->order_by('tgl_trans', 'DESC');
        $this->db->group_by('WEEK(tgl_trans)');
        $data = $this->db->get();

        return $data;
    }

    public function hapusTrans($id){
        $this->db->where('id_trans', $id);
        $delete = $this->db->delete('transaksi');
        return $delete;
    }

    public function ubahTransaksi($id, $data){
        $this->db->where('id_trans', $id);
        $update = $this->db->update('transaksi', $data);

        return $update;
    }

    public function getTotalPendapatanTerakhir(){
        $this->db->select('*');
        $this->db->select_sum('total');
        $this->db->select_sum('qty');
        $this->db->from('transaksi');
        $this->db->join('user', 'transaksi.user_id = user.id');
        $this->db->join('detail_trans', 'transaksi.id_trans = detail_trans.trans_id');
        $this->db->order_by('tgl_trans', 'DESC');
        $this->db->group_by('WEEK(tgl_trans)');
        $data = $this->db->get();

        return $data;
    }

}