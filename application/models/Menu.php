<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/24/2019
 * Time: 6:41 PM
 */

class Menu extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Resep');
        $this->load->model('Stok');
    }

    public function getMenuAll(){
        $this->db->select('*');
        $this->db->from('menu');
        return $this->db->get();
    }

    public function getMenuPerId($id){
        $this->db->select('*');
        $this->db->from('menu');
        $this->db->where('id_menu', $id);
        return $this->db->get();
    }

    public function tambahMenu($data){
        $this->db->insert('menu', $data);
        $id = $this->db->insert_id();

        return $id;
    }

    public function updateMenu($data, $id){
        $this->db->where('id_menu', $id);
        $update = $this->db->update('menu', $data);

        return $update;
    }

    public function hapusMenu($id){
        $this->db->where('id_menu', $id);
        $this->db->delete('menu');
    }

    public function getStokMenu($id){
        $resep = $this->Resep->getResepPerMenu($id)->result();
        foreach ($resep as $r){
            $stok = $this->Stok->getJumlahStokPerBahanbaku($r->bahanbaku_id);
            $spt[$r->id_resep] = $stok/$r->takaran;
        }
        $jml_stok = floor(min($spt));

        return $jml_stok;
    }
}