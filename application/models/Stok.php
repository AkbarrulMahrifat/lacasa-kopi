<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/16/2019
 * Time: 6:25 PM
 */

class Stok extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getStokAll(){
        $this->db->select('*');
        $this->db->from('stok');
        $this->db->join('bahanbaku', 'stok.bahanbaku_id = bahanbaku.id_bahanbaku');
        $this->db->order_by('tgl_beli', 'DESC');
        return $this->db->get();
    }

    public function getJumlahStok(){
        $this->db->select('*');
        $this->db->select_sum('sisa');
        $this->db->from('stok');
        $this->db->where('status', 1);
        $this->db->group_by('bahanbaku_id');
        return $this->db->get();
    }

    public function getJumlahStokPerBahanbaku($id_bahanbaku){
        $this->db->select_sum('sisa');
        $this->db->from('stok');
        $this->db->where('bahanbaku_id', $id_bahanbaku);
        $this->db->where('status', 1);
        return $this->db->get()->row('sisa');
    }

    public function tambahStok($data){
        $insert = $this->db->insert('stok', $data);

        return $insert;
    }

    public function updateStok($data, $id){
        $this->db->where('id_stok', $id);
        $insert = $this->db->update('stok', $data);

        return $insert;
    }

    public function hapusStok($id){
        $this->db->where('id_stok', $id);
        $this->db->delete('stok');
    }

    public function getStokPerId($id){
        $this->db->select('*');
        $this->db->from('stok');
        $this->db->where('bahanbaku_id', $id);
        $this->db->where('status', 1);
        $this->db->order_by('tgl_kadaluarsa', 'ASC');
        return $this->db->get();
    }
}