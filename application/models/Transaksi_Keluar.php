<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/20/2019
 * Time: 1:27 PM
 */

class Transaksi_Keluar extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getTransKeluarAll(){
        $this->db->select('*');
        $this->db->from('transaksi_keluar');
        $this->db->join('user', 'transaksi_keluar.user_id = user.id');
        $this->db->order_by('tgl_transkeluar', 'DESC');
        return $this->db->get();
    }

    public function getTransaksiPerId($id){
        $this->db->select('*');
        $this->db->from('transaksi_keluar');
        $this->db->where('user_id', $id);
        return $this->db->get();
    }

    public function tambahTransaksiKeluar($data){
        $insert = $this->db->insert('transaksi_keluar', $data);

        return $insert;
    }

    public function ubahTransaksiKeluar($id,$data){
        $this->db->where('id_transkeluar', $id);
        $insert = $this->db->update('transaksi_keluar', $data);

        return $insert;
    }

    public function hapusTransaksiKeluar($id){
        $this->db->where('id_transkeluar', $id);
        $delete = $this->db->delete('transaksi_keluar');

        return $delete;
    }

    public function getTotalPengeluaranTerakhir(){
        $this->db->select('*');
        $this->db->select_sum('total');
        $this->db->from('transaksi_keluar');
        $this->db->join('user', 'transaksi_keluar.user_id = user.id');
        $this->db->order_by('tgl_transkeluar', 'DESC');
        $this->db->group_by('WEEK(tgl_transkeluar)');
        $data = $this->db->get();

        return $data;
    }
}