<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/16/2019
 * Time: 6:25 PM
 */

class Bahanbaku extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getBahanbakuAll(){
        $this->db->select('*');
        $this->db->from('bahanbaku');
        return $this->db->get();
    }

    public function getBahanbakuPerId($id){
        $this->db->select('*');
        $this->db->from('bahanbaku');
        $this->db->where('id_bahanbaku', $id);
        return $this->db->get();
    }

    public function tambahBahanbaku($data){
        $insert = $this->db->insert('bahanbaku', $data);

        return $insert;
    }

    public function hapusBahanbaku($id){
        $this->db->where('id_bahanbaku', $id);
        $delete = $this->db->delete('bahanbaku');
        return $delete;
    }

    public function updateBahanbaku($data,$id){
        $this->db->where('id_bahanbaku', $id);
        $update = $this->db->update('bahanbaku', $data);
        return $update;
    }

    public function getBahanbakuKopi(){
        $this->db->select('*');
        $this->db->from('bahanbaku');
        $this->db->where('jenis_bahanbaku', 1);
        return $this->db->get();
    }
}