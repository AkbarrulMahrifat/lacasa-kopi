<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 5/27/2019
 * Time: 12:28 AM
 */

class Detail_trans extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function tambahKeranjang($data){
        $insert = $this->db->insert('detail_trans', $data);

        return $insert;
    }

    public function getKeranjang(){
        $this->db->select('*');
        $this->db->from('detail_trans');
        $this->db->join('menu', 'detail_trans.menu_id = menu.id_menu');
        $this->db->where('trans_id', NULL);
        return $this->db->get();
    }

    public function getDetailTransPerIdTrans($id){
        $this->db->select('*');
        $this->db->from('detail_trans');
        $this->db->join('menu', 'detail_trans.menu_id = menu.id_menu');
        $this->db->where('trans_id', $id);
        return $this->db->get();
    }

    public function updateDetailTrans($data, $id_menu, $qty){
        $this->db->where('trans_id', NULL);
        $this->db->where('menu_id', $id_menu);
        $this->db->where('qty', $qty);
        $update = $this->db->update('detail_trans', $data);

        return $update;
    }

    public function hapusDetailTrans($id){
        $this->db->where('id_detailtrans', $id);
        $delete = $this->db->delete('detail_trans');
        return $delete;
    }

    public function ubahStatusDetail($id, $status){
        $this->db->where('id_detailtrans', $id);
        $update = $this->db->update('detail_trans', array('status' => $status));

        return $update;
    }
}