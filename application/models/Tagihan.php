<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 6/6/2019
 * Time: 12:42 AM
 */

class Tagihan extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getTagihanAll(){
        $this->db->select('*');
        $this->db->from('tagihan');
        $this->db->order_by('tgl_jatuhtempo', 'DESC');
        $this->db->order_by('status', 'ASC');
        return $this->db->get();
    }

    public function tambahTagihan($data){
        $insert = $this->db->insert('tagihan', $data);

        return $insert;
    }

    public function updateTagihan($data, $id){
        $this->db->where('id_tagihan', $id);
        $update = $this->db->update('tagihan', $data);

        return $update;
    }

    public function hapusTagihan($id){
        $this->db->where('id_tagihan', $id);
        $this->db->delete('tagihan');
    }
}