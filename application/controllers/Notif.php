<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 7/10/2019
 * Time: 10:57 PM
 */

class Notif extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('User');
        $this->load->model('Bahanbaku');
        $this->load->model('Stok');
        $this->load->model('Transaksi_Keluar');
        $this->load->model('Menu');
        $this->load->model('Resep');
        $this->load->model('Transaksi');
        $this->load->model('Detail_trans');
        $this->load->model('Tagihan');
        $this->load->model('Peramalan');
    }

    public function getNotif(){
        $stokKadaluarsa = $this->Stok->getStokAll()->result_array();
        $a = array();
        for ($i=0; $i<count($stokKadaluarsa); $i++){
            $batasKadaluarsa[$i] = date('Y-m-d', strtotime('-7 day', strtotime($stokKadaluarsa[$i]['tgl_kadaluarsa'])));
            if ($batasKadaluarsa[$i] <= date('Y-m-d') && $stokKadaluarsa[$i]['tgl_kadaluarsa'] >= $batasKadaluarsa[$i] && $stokKadaluarsa[$i]['status'] == 1){
                $a[] = "<a href=\"#\" class=\"list-group-item list-group-item-action\">
                        <div class=\"notification-info\">
                            <div class=\"notification-list-user-img\"><img src=\"\" alt=\"\" class=\"user-avatar-md rounded-circle\"></div>
                                <div class=\"notification-list-user-block\"> Stok bahanbaku <span class=\"notification-list-user-name\">".$stokKadaluarsa[$i]['nama_bahanbaku']."</span> akan kadaluarsa pada tanggal ".date('d M Y', strtotime($stokKadaluarsa[$i]['tgl_kadaluarsa'])).".</div>
                         </div>
                      </a>";
            }
        }
        $stok = $this->Stok->getJumlahStok()->result_array();
        for ($i=0; $i<count($stok); $i++){
            if ($stok[$i]['sisa'] <= 75){
                $a[] = "<a href=\"#\" class=\"list-group-item list-group-item-action\">
                        <div class=\"notification-info\">
                            <div class=\"notification-list-user-img\"><img src=\"\" alt=\"\" class=\"user-avatar-md rounded-circle\"></div>
                                <div class=\"notification-list-user-block\"> Stok bahanbaku <span class=\"notification-list-user-name\">".$stokKadaluarsa[$i]['nama_bahanbaku']."</span> hampir habis ! Segera cek ketersediaan stok.</div>
                         </div>
                      </a>";
            }
        }

        $data = json_encode($a);
        echo $data;
    }

    public function ubahStatusOtomatis(){
        $data = $this->Stok->getStokAll()->result_array();
        for ($i=0; $i<count($data); $i++){
            if ($data[$i]['tgl_kadaluarsa'] <= date('Y-m-d')){
                $editKadaluarsa[$i] = array('status' => 3);
                $ubah[$i] = $this->Stok->updateStok($editKadaluarsa[$i], $data[$i]['id_stok']);
            }

            if ($data[$i]['sisa'] <= 0){
                $editHabis[$i] = array('sisa' => 0, 'status' => 2);
                $ubah[$i] = $this->Stok->updateStok($editHabis[$i], $data[$i]['id_stok']);
            }
        }

        return TRUE;
    }
}