<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/12/2019
 * Time: 10:39 PM
 */

class Pegawai extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('jabatan') != 2 or $this->session->userdata('jabatan') == NULL){
            $this->session->set_flashdata('error', 'Silahkan login terlebih dahulu !');
            redirect('/');
        }

        $this->load->model('User');
        $this->load->model('Bahanbaku');
        $this->load->model('Stok');
        $this->load->model('Transaksi_Keluar');
        $this->load->model('Menu');
        $this->load->model('Resep');
        $this->load->model('Transaksi');
        $this->load->model('Detail_trans');
        $this->load->model('Tagihan');
        $this->load->model('Peramalan');
    }

    public function beranda(){
        if ($this->session->userdata('penjualan') == 1){
            redirect('Pegawai/kasir');
        }elseif ($this->session->userdata('stok') == 1){
            redirect('Pegawai/stok');
        }elseif ($this->session->userdata('keuangan') == 1){
            redirect('Pegawai/transaksi_masuk');
        }
    }

    public function getGrafikPenjualanPeramalan($id){
        $from = $this->Transaksi->getTransaksiAll()->row()->tgl_trans;
        $getAktual = $this->Transaksi->getTransaksiPerBahanbakuPerOrderLimit($id, NULL, "DESC")->result_array();
        $getPeramalan = $this->Peramalan->getPeramalanPerIdBB($id)->result_array();

        //ini ambil periodenya
        $periode = array();
        $awal = date('Y-m-d', strtotime($from));

        for ($i=0; $i < count($getAktual); $i++) {
            $periode[$i] = $awal;
            $awal = date('Y-m-d', strtotime("-7 day", strtotime(date($awal))));
        }

        $aktual = array();
        for ($i = 0; $i < count($periode); $i++) {
            for ($j = 0; $j < count($getAktual); $j++) {
                if ($periode[$i] == $getAktual[$j]['tgl_trans']) {
                    $aktual[$i] = intval($getAktual[$j]['qty']);
                    break;
                } else {
                    $aktual[$i] = 0;
                }
            }
        }

        $peramalan = array();
        for ($i = 0; $i < count($periode); $i++) {
            for ($j = 0; $j < count($getPeramalan); $j++) {
                if ($periode[$i] == $getPeramalan[$j]['periode']) {
                    $peramalan[$i] = $getPeramalan[$j]['hasil'];
                    break;
                } else {
                    $peramalan[$i] = 0;
                }
            }
        }

        $data['aktual'] = $aktual;
        $data['peramalan'] = $peramalan;
        $data['periode'] = $periode;

//        if(isset($data)) {
//            $a = json_encode($data);
//        }else{
//            $a = '';
//        }

        echo json_encode($data);
    }

    //Penjualan
    public function kasir(){
        $menu = $this->Menu->getMenuAll()->result();
        $dataMenu = array();
        $i=0;
        foreach ($menu as $m){
            $jml_stok[$m->id_menu] = $this->Menu->getStokMenu($m->id_menu);

            if($jml_stok[$m->id_menu] == NULL){
                $jml_stok[$m->id_menu] = 0;
            };
            $dataMenu[$i] = array(
                'id_menu' => $m->id_menu,
                'nama_menu' => $m->nama_menu,
                'harga' => $m->harga,
                'gambar' => $m->gambar,
                'stok' => $jml_stok[$m->id_menu]
            );
            $i++;
        }

        $data['keranjang'] = $this->Detail_trans->getKeranjang()->result();
        $data['menu'] = $dataMenu;

        $total = array();
        $i=0;
        foreach ($data['keranjang'] as $k){
            $total[$i++] = $k->qty * $k->harga;
        }

        $data['total'] = array_sum($total);
        $data['transaksi'] = $this->Transaksi->getTransaksiHariIni()->result();

        $this->load->view('header.php', $data);
        $this->load->view('Pegawai/kasir.php');
        $this->load->view('footer.php');
    }

    public function tambahKeranjang(){
        $input = $this->input->post();
        $data = array(
            'trans_id' => NULL,
            'menu_id' => $input['id_menu'],
            'qty' => $input['jumlah_menu'],
            'status' => 1,
        );

        $insert = $this->Detail_trans->tambahKeranjang($data);
        if ($insert == TRUE){
            $this->session->set_flashdata('success', 'Berhasil menambahkan produk ke keranjang.');
        }else{
            $this->session->set_flashdata('error', 'Gagal menambahkan produk ke keranjang.');
        }
        redirect('Pegawai/kasir');
    }

    public function hapusKeranjang($id){
        $delete = $this->Detail_trans->hapusDetailTrans($id);
        if ($delete == TRUE){
            $this->session->set_flashdata('success', 'Berhasil menghapus data.');
            redirect('Pegawai/kasir');
        }else{
            $this->session->set_flashdata('error', 'Gagal menghapus data.');
            redirect('Pegawai/kasir');
        }
    }

    public function tambahTransaksi(){
        $input = $this->input->post();

        $trans = array(
            'user_id' => $this->session->userdata('id'),
            'tgl_trans' => $input['tgl_trans'],
            'total' => $input['total'],
            'bayar' => $input['bayar'],
            'kembali' => $input['kembali'],
            'nomeja' => $input['nomeja'],
            'status' => $input['status'],
        );

        $this->db->trans_begin();
        $insertTrans = $this->Transaksi->tambahTrans($trans);

        $detailTrans=array();
        for ($i=0; $i<count($input['menu_id']); $i++){
            $detailTrans[$i] = array(
                'trans_id' => $insertTrans,
                'menu_id' => $input['menu_id'][$i],
                'qty' => $input['qty'][$i],
                'status' => $input['statusPesanan'][$i],
            );
            $updateDetail = $this->Detail_trans->updateDetailTrans($detailTrans[$i], $input['menu_id'][$i], $input['qty'][$i]);

            //update stok
            $getResep[$i] = $this->Resep->getResepPerMenu($input['menu_id'][$i])->result();
            foreach ($getResep[$i] as $r){
                $getStok[$r->id_resep] = $this->Stok->getStokPerId($r->bahanbaku_id)->result_array();

                for ($j=0; $j<count($getStok[$r->id_resep]); $j++){
                    $kurangiStok[$j] = $getStok[$r->id_resep][$j]['sisa']-($input['qty'][$i]*$r->takaran);
                    if ($kurangiStok[$j] < 0){
                        $updateStok = $this->Stok->updateStok(array('sisa'=>0, 'status'=>2), $getStok[$r->id_resep][$j]['id_stok']);
                        continue;
                    }else{
                        if (count($getStok[$r->id_resep]) > 1){
                            $kurangiStok[$j] = $getStok[$r->id_resep][$j]['sisa']-abs($kurangiStok[$j-1]);
                            $updateStok = $this->Stok->updateStok(array('sisa'=>$kurangiStok[$j]), $getStok[$r->id_resep][$j]['id_stok']);
                        }else{
                            $updateStok = $this->Stok->updateStok(array('sisa'=>$kurangiStok[$j]), $getStok[$r->id_resep][$j]['id_stok']);
                        }
                        break;
                    }
                }
            }
        }


        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->session->set_flashdata('error', 'Gagal menambah transaksi. Silahkan ulangi lagi.');
        }else{
            $this->db->trans_commit();
            $this->session->set_flashdata('success', 'Transaksi berhasil dilakukan.');
        }
        redirect('Pegawai/kasir');
    }

    public function getDetailTransKasir($id_transaksi){
        $detailTrans = $this->Detail_trans->getDetailTransPerIdTrans($id_transaksi)->result();
        $confirm = "return confirm('Apakah anda yakin ingin mengubah status data ini ?')";
        $no = 1;
        $i=0;
        foreach ($detailTrans as $dt){
            if ($dt->status == 1){
                $option1 = "selected";
                $option2 = "";

            }else{
                $option1 = "";
                $option2 = "selected";
            }
            echo '<tr>
                        <td>
                            '.$no++.'
                            <input type="hidden" name="id_detail['.$dt->id_detailtrans.']" value="'.$dt->id_detailtrans.'">
                        </td>
                        <td>'.$dt->nama_menu.'</td>
                        <td>'.$dt->qty.'</td>
                        <td>';
            echo '
                            <select class="form-control" id="statuspesanan" aria-describedby="inputGroupPrepend" name="status_pesanan['.$dt->id_detailtrans.']" required>
                                <option value="">Pilih Status</option>
                                <option value="1" '.$option1.'>Diproses</option>
                                <option value="2" '.$option2.'>Selesai</option>
                            </select>
                        </td>
                  </tr>';
        }
    }

    public function hapusTransaksi($id){
        $hapus = $this->Transaksi->hapusTrans($id);

        if($hapus == TRUE){
            $this->session->set_flashdata('success', 'Berhasil menghapus status data.');
        }else{
            $this->session->set_flashdata('error', 'Gagal menghapus data. Silahkan ulangi lagi.');
        }
        redirect('Pegawai/kasir');
    }

    public function ubahTransaksi(){
        $input = $this->input->post();
        $id = $input['id_trans'];
        $kembali = $input['kembali'];
        if ($input['kembali'] == NULL){
            $kembali = $input['kembalianlama'];
        }
        $data = array(
            'tgl_trans' => $input['tgl_trans'],
            'total' => $input['total'],
            'bayar' => $input['bayar'],
            'kembali' => $kembali,
            'nomeja' => $input['nomeja'],
            'status' => $input['status'],
        );

        $this->db->trans_begin();
        $updateTrans = $this->Transaksi->ubahTransaksi($id, $data);

        foreach ($input['id_detail'] as $a){
            $updateDetail = $this->Detail_trans->ubahStatusDetail($input['id_detail'][$a], $input['status_pesanan'][$a]);
        }

        if($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $this->session->set_flashdata('success', 'Berhasil mengubah data.');
        }else{
            $this->db->trans_rollback();
            $this->session->set_flashdata('error', 'Gagal mengubah data. Silahkan ulangi lagi.');
        }
//        redirect('Pegawai/kasir');
        echo '<script>window.location.reload(history.back())</script>';
    }

    public function daftarPenjualan(){
        $data['penjualan'] = $this->Transaksi->getTransaksiAll()->result_array();

        $this->load->view('header.php', $data);
        $this->load->view('Pegawai/daftarpenjualan.php');
        $this->load->view('footer.php');
    }

    public function getDetailTrans($id){
        $trans = $this->Transaksi->getTransaksiPerId($id)->row_array();
        $detailtrans = $this->Detail_trans->getDetailTransPerIdTrans($id)->result();
        $no = 1;

        echo '<div class="card" style="margin-bottom: 0px">
                                    <div class="card-header p-4">
                                        <img src="'.base_url().'/assets/assets/images/LOGO LA CASA.png" style="width: 85px">
                                        <div class="col-sm-4 float-right ">
                                            <h3 class="mb-0">Pesanan #'.$trans['id_trans'].'</h3>
                                            <a>'.date('d M Y', strtotime($trans['tgl_trans'])).'</a><br>
                                            <a>Meja '.$trans['nomeja'].'</a>
                                        </div>
                                    </div>
                                    <div class="card-body" style="padding: 0px 20px 0px 20px">
                                        <div class="table-responsive-sm">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th class="center">#</th>
                                                    <th>Menu</th>
                                                    <th class="right">Harga</th>
                                                    <th class="center">Qty</th>
                                                    <th class="right">Total</th>
                                                </tr>
                                                </thead>
                                                <tbody>';
        foreach ($detailtrans as $dt) {
            echo '<tr>
                                                    <td class="center">'.$no++.'</td>
                                                    <td class="left strong">'.$dt->nama_menu.'</td>
                                                    <td class="right">Rp '.number_format($dt->harga, '0', ',', '.').'</td>
                                                    <td class="center">'.$dt->qty.'</td>
                                                    <td class="right">Rp. '.number_format(($dt->harga*$dt->qty), '0', ',', '.').'</td>
                                                </tr>';
        }
        echo '                                  
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-7 col-sm-7">
                                            </div>
                                            <div class="col-lg-5 col-sm-5">
                                                <table class="table table-clear">
                                                    <tbody>
                                                    <tr>
                                                        <td class="left">
                                                            <strong class="text-dark">Total</strong>
                                                        </td>
                                                        <td class="right">Rp. '.number_format($trans['total'], '0', ',', '.').'</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="left">
                                                            <strong class="text-dark">Bayar</strong>
                                                        </td>
                                                        <td class="right">Rp. '.number_format($trans['bayar'], '0', ',', '.').'</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="left">
                                                            <strong class="text-dark">Kembali</strong>
                                                        </td>
                                                        <td class="right">Rp. '.number_format($trans['kembali'], '0', ',', '.').'</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>';
    }


    //stok bahanbaku
    public function stok(){
        $bahanbaku = $this->Bahanbaku->getBahanbakuAll()->result();
        $stok = array();
        $i = 0;
        foreach ($bahanbaku as $bb){
            $jml_stok[$i] = $this->Stok->getJumlahStokPerBahanbaku($bb->id_bahanbaku);
            $rek_stok[$i] = $this->Peramalan->getPeramalanPerIdBB($bb->id_bahanbaku)->row();
            if($jml_stok[$i] == NULL){
                $jml_stok[$i] = 0;
            };
            if($rek_stok[$i] == NULL){
                $rek_stok[$i] = 0;
            }else{
                $rek_stok[$i] = $rek_stok[$i]->rekomendasi_stok;
            };
            $stok[$i] = array(
                'id_bahanbaku' => $bb->id_bahanbaku,
                'nama_bahanbaku' => $bb->nama_bahanbaku,
                'harga_bahanbaku' => $bb->harga_bahanbaku,
                'satuan' => $bb->satuan,
                'stok' => $jml_stok[$i],
                'rek_stok' => $rek_stok[$i],
                'jenis_bahanbaku' => $bb->jenis_bahanbaku,
            );
            $i++;
        }
        $data['bahanbaku'] = $stok;

        $this->load->view('header.php', $data);
        $this->load->view('Pegawai/stok.php');
        $this->load->view('footer.php');
    }

    public function tambahBahanbaku(){
        $data = array(
            'nama_bahanbaku' => $this->input->post('nama'),
            'harga_bahanbaku' => $this->input->post('harga'),
            'satuan' => $this->input->post('satuan'),
            'jenis_bahanbaku' => $this->input->post('jenis_bahanbaku'),
        );
        $this->Bahanbaku->tambahBahanbaku($data);
        $this->session->set_flashdata('success', 'Berhasil menambahkan data.');
        redirect('Pegawai/stok');
    }

    public function tambahStok(){
        $input = $this->input->post();

        $bahanbaku = $this->Bahanbaku->getBahanbakuPerId($input['id_bahanbaku'])->row_array();
        $now = date('Y-m-d');

        $stok = array(
            'bahanbaku_id' => $input['id_bahanbaku'],
            'tgl_beli' => $input['tgl_beli'],
            'tgl_kadaluarsa' => $input['tgl_kadaluarsa'],
            'jumlah_beli' => $input['stok'],
            'sisa' => $input['stok'],
            'status' => 1,
        );
        $trans = array(
            'user_id' => $this->session->userdata('id'),
            'tgl_transkeluar' => $now,
            'nama_transkeluar' => "Belanja ".$bahanbaku['nama_bahanbaku'],
            'total' => $bahanbaku['harga_bahanbaku'] * $input['stok'],
            'jenis_transkeluar' => "Belanja",
        );

        $this->db->trans_begin();
        $this->Stok->tambahStok($stok);
        $this->Transaksi_Keluar->tambahTransaksiKeluar($trans);

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->session->set_flashdata('error', 'Gagal menambahkan data. Silahkan ulangi lagi.');
            redirect('Pegawai/stok');
        }else{
            $this->db->trans_commit();
            $this->session->set_flashdata('success', 'Berhasil menambahkan data.');
            redirect('Pegawai/stok');
        }
    }

    public function editbahanbaku(){
        $input = $this->input->post();
        $data = array(
            'nama_bahanbaku' => $input['nama'],
            'harga_bahanbaku' => $input['harga'],
            'satuan' => $input['satuan'],
            'jenis_bahanbaku' => $input['jenis_bahanbaku'],
        );
        $update = $this->Bahanbaku->updateBahanbaku($data, $input['id']);
        if ($update == TRUE){
            $this->session->set_flashdata('success', 'Berhasil mengubah data.');
        }else{
            $this->session->set_flashdata('error', 'Gagal mengubah data.');
        }
        redirect('Pegawai/stok');
    }

    public function hapusBahanbaku($id){
        $delete = $this->Bahanbaku->hapusBahanbaku($id);

        if ($delete == TRUE){
            $this->session->set_flashdata('success', 'Berhasil menghapus data.');
            redirect('Pegawai/stok');
        }else{
            $this->session->set_flashdata('error', 'Gagal menghapus data.');
            redirect('Pegawai/stok');
        }
    }

    public function riwayat_stok(){
        $data['bahanbaku'] = $this->Bahanbaku->getBahanbakuAll()->result();
        $data['riwayatstok'] = $this->Stok->getStokAll()->result();

        $this->load->view('header.php', $data);
        $this->load->view('Pegawai/riwayatstok.php');
        $this->load->view('footer.php');
    }

    public function updateStok(){
        $input = $this->input->post();
        $data = array(
            'bahanbaku_id' => $input['bahanbaku_id'],
            'tgl_beli' => $input['tgl_beli'],
            'tgl_kadaluarsa' => $input['tgl_kadaluarsa'],
            'jumlah_beli' => $input['jumlah_beli'],
            'sisa' => $input['sisa'],
            'status' => $input['status'],
        );
        $update = $this->Stok->updateStok($data, $input['id_stok']);
        if ($update == TRUE){
            $this->session->set_flashdata('success', 'Berhasil mengubah data.');
        }else{
            $this->session->set_flashdata('error', 'Gagal mengubah data.');
        }
        redirect('Pegawai/riwayat_stok');
    }

    public function hapusStok($id){
        $delete = $this->Stok->hapusStok($id);

        if ($delete == TRUE){
            $this->session->set_flashdata('success', 'Berhasil menghapus data.');
            redirect('Pegawai/riwayat_stok');
        }else{
            $this->session->set_flashdata('error', 'Gagal menghapus data.');
            redirect('Pegawai/riwayat_stok');
        }
    }

    //Menu
    public function menu(){
        $data['menu'] = $this->Menu->getMenuAll()->result();
        $data['bahanbaku'] = $this->Bahanbaku->getBahanbakuAll()->result();
        $this->load->view('header.php', $data);
        $this->load->view('Pegawai/menu.php');
        $this->load->view('footer.php');
    }

    public function tambahMenu(){
        $input = $this->input->post();

        //proses konfigurasi gambar
        $config['upload_path']          = './assets/FotoMenu/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
        $config['max_size']             = 0;

        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload('gambar');
        $uploaddata = $this->upload->data();
        $nama = $uploaddata['file_name'];

        if ($nama != ""){
            $gambar = $nama;
        }else{
            $gambar = NULL;
        }

        $menu = array(
            'nama_menu' => $input['nama_menu'],
            'harga' => $input['harga_menu'],
            'gambar' => $gambar,
        );

        $this->db->trans_begin();
        $insertMenu = $this->Menu->tambahMenu($menu);

        $resep=array();
        for ($i=0; $i<count($input['resep']); $i++){
            $resep[$i] = array(
                'menu_id' => $insertMenu,
                'bahanbaku_id' => $input['resep'][$i],
                'takaran' => $input['takaran'][$i],
            );
        }

        $insertResep = $this->Resep->tambahResep($resep);

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->session->set_flashdata('error', 'Gagal menambahkan data. Silahkan ulangi lagi.');
            redirect('Pegawai/menu');
        }else{
            $this->db->trans_commit();
            $this->session->set_flashdata('success', 'Berhasil menambahkan data.');
            redirect('Pegawai/menu');
        }
    }

    public function updateMenu(){
        $input = $this->input->post();

        //proses konfigurasi gambar
        $config['upload_path']          = './assets/FotoMenu/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
        $config['max_size']             = 0;

        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload('gambar');
        $uploaddata = $this->upload->data();
        $nama = $uploaddata['file_name'];

        if ($nama != ""){
            $gambar = $nama;
            unlink("assets/FotoMenu/".$input['gambarlama']);
        }else{
            $gambar = $input['gambarlama'];
        }

        $menu = array(
            'nama_menu' => $input['nama_menu'],
            'harga' => $input['harga_menu'],
            'gambar' => $gambar,
        );

        $update = $this->Menu->updateMenu($menu, $input['id_menu']);
        if ($update == TRUE){
            $this->session->set_flashdata('success', 'Berhasil mengubah data.');
        }else{
            $this->session->set_flashdata('error', 'Gagal mengubah data.');
        }
        redirect('Pegawai/menu');
    }

    public function hapusMenu($id){
        $menu = $this->Menu->getMenuPerId($id)->row_array();
        $this->db->trans_begin();
        $delete = $this->Menu->hapusMenu($id);
        unlink("assets/FotoMenu/".$menu['gambar']);

        if ($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $this->session->set_flashdata('success', 'Berhasil menghapus data.');
            redirect('Pegawai/menu');
        }else{
            $this->db->trans_rollback();
            $this->session->set_flashdata('error', 'Gagal menghapus data.');
            redirect('Pegawai/menu');
        }
    }

    //Resep
    public function resep(){
        $data['resep'] = $this->Resep->getResepAll()->result();
        $data['menu'] = $this->Menu->getMenuAll()->result();
        $data['bahanbaku'] = $this->Bahanbaku->getBahanbakuAll()->result();

        $this->load->view('header.php', $data);
        $this->load->view('Pegawai/resep.php');
        $this->load->view('footer.php');
    }

    public function getResepMenuPerId($id_menu){
        $resep = $this->Resep->getResepPerMenu($id_menu)->result();
        if (count($resep) == 0){
            echo '<p class="card-text">Belum ada resep</p>';
        }else{
            echo '<ul>';
            foreach ($resep as $r){
                echo '<li>'.$r->nama_bahanbaku.' ('.$r->takaran.' '.$r->satuan.')</li>';
            }
            echo '</ul>';
        }
    }

    public function getResep($id_menu)
    {
        $confirm = "return confirm('Apakah anda yakin ingin menghapus data ini ?')";
        $tambahResep = "tambahResep()";
        $resep = $this->Resep->getResepPerMenu($id_menu)->result();
        $no = 1;
        if (count($resep) == 0){
            echo '<table class="table table-striped table-bordered" style="width:100%">
                    <thead>
                    <tr>
                    <th>#</th>
                    <th>Nama Bahanbaku</th>
                    <th>Takaran</th>
                    <th style="width: 150px">Aksi</th>
                    </tr>
                    </thead>
                    <tbody id="daftarResep">
                    <tr class="text-center">
                    <td colspan="4">Tidak ada data</td>
                    </tr>
                    </tbody>
                  </table>';
        }else{
            echo '<table class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        <tr>
                        <th>#</th>
                        <th>Nama Bahanbaku</th>
                        <th>Takaran</th>
                        <th style="width: 150px">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>';
            foreach ($resep as $r) {
                echo '<tr>
                        <td>'.$no++.'</td>
                        <td>'.$r->nama_bahanbaku.'</td>
                        <td>'.$r->takaran.' '.$r->satuan.'</td>
                        <td>
                        <a class="btn btn-xs btn-info editResep text-light" data-idresep="'.$r->id_resep.'" data-idbahanbaku="'.$r->bahanbaku_id.'" data-takaran="'.$r->takaran.'"><i class="fa fa-edit"></i> </a>
                        <a class="btn btn-xs btn-danger" onclick="'.$confirm.'" href="'.site_url('Pegawai/hapusResep/'.$r->id_resep).'"><i class="fa fa-trash"></i></a>
                        </td>
                        </tr>';
            }
            echo '</tbody>
                      </table>
                      <br>
                      <a class="btn btn-sm btn-success text-light tambahResep" data-idmenu="'.$r->menu_id.'"><i class="fa fa-plus"></i> </a>';

        }
    }

    public function tambahResep(){
        $input = $this->input->post();
        $resep=array();
        for ($i=0; $i<count($input['resep']); $i++){
            $resep[$i] = array(
                'menu_id' => $input['menu_id'],
                'bahanbaku_id' => $input['resep'][$i],
                'takaran' => $input['takaran'][$i],
            );
        }

        $insertResep = $this->Resep->tambahResep($resep);

        if($insertResep == FALSE){
            $this->session->set_flashdata('error', 'Gagal menambahkan data. Silahkan ulangi lagi.');
            redirect('Pegawai/resep');
        }else{
            $this->session->set_flashdata('success', 'Berhasil menambahkan data.');
            redirect('Pegawai/resep');
        }
    }

    public function updateResep(){
        $input = $this->input->post();
        $data = array(
            'bahanbaku_id' => $input['bahanbaku_id'],
            'takaran' => $input['takaran'],
        );
        $update = $this->Resep->updateResep($data, $input['id_resep']);
        if ($update == TRUE){
            $this->session->set_flashdata('success', 'Berhasil mengubah data.');
        }else{
            $this->session->set_flashdata('error', 'Gagal mengubah data.');
        }
        redirect('Pegawai/resep');
    }

    public function hapusResep($id){
        $delete = $this->Resep->hapusResep($id);

        if ($delete == TRUE){
            $this->session->set_flashdata('success', 'Berhasil menghapus data.');
            redirect('Pegawai/resep');
        }else{
            $this->session->set_flashdata('error', 'Gagal menghapus data.');
            redirect('Pegawai/resep');
        }
    }

    //Transaksi
    public function transaksi_masuk(){
        $data['transaksi'] = $this->Transaksi->getTransaksiAll()->result_array();
        $this->load->view('header.php', $data);
        $this->load->view('Pegawai/transaksimasuk.php');
        $this->load->view('footer.php');
    }

    public function transaksi_keluar(){
        $data['transaksi'] = $this->Transaksi_Keluar->getTransKeluarAll()->result_array();
        $this->load->view('header.php', $data);
        $this->load->view('Pegawai/transaksikeluar.php');
        $this->load->view('footer.php');
    }

    //Tagihan
    public function tagihan(){
        $data['tagihan'] = $this->Tagihan->getTagihanAll()->result();
        $this->load->view('header.php', $data);
        $this->load->view('Pegawai/daftartagihan.php');
        $this->load->view('footer.php');
    }

    public function tambahTagihan(){
        $input = $this->input->post();

        $data= array(
            'nama_tagihan' => $input['nama_tagihan'],
            'nominal' => $input['nominal'],
            'sisa' => $input['nominal'],
            'tgl_jatuhtempo' => $input['tgl_jatuhtempo'],
            'jenis_tagihan' => $input['jenis_tagihan'],
        );

        $insert = $this->Tagihan->tambahTagihan($data);

        if ($insert == TRUE){
            $this->session->set_flashdata('success', 'Berhasil menambah tagihan.');
            redirect('Pegawai/tagihan');
        }else{
            $this->session->set_flashdata('error', 'Gagal menambah tagihan.');
            redirect('Pegawai/tagihan');
        }
    }

    public function editTagihan(){
        $input = $this->input->post();
        $sisa = ($input['nominal']-$input['nominallama']) + $input['sisa'];
        if ($sisa <= 0){
            $status = 1;
            $sisa = 0;
        }else{
            $status = 0;
        }
        $data = array(
            'nama_tagihan' => $input['nama_tagihan'],
            'nominal' => $input['nominal'],
            'sisa' => $sisa,
            'tgl_jatuhtempo' => $input['tgl_jatuhtempo'],
            'jenis_tagihan' => $input['jenis_tagihan'],
            'status' => $status,
        );

        $update = $this->Tagihan->updateTagihan($data, $input['id_tagihan']);
        if ($update == TRUE){
            $this->session->set_flashdata('success', 'Berhasil mengubah data tagihan.');
        }else{
            $this->session->set_flashdata('error', 'Gagal mengubah data tagihan.');
        }
        redirect('Pegawai/tagihan');
    }

    public function bayarTagihan(){
        $input = $this->input->post();
        $sisa = ($input['nominal']-$input['pembayaran']);
        if ($sisa <= 0){
            $status = 1;
            $sisa = 0;
        }else{
            $status = 0;
        }
        $data = array(
            'nominal' => $input['nominal'],
            'sisa' => $sisa,
            'status' => $status,
        );
        $trans = array(
            'user_id' => $this->session->userdata('id'),
            'tgl_transkeluar' => date('Y-m-d H:i:s'),
            'nama_transkeluar' => 'Pembayaran tagihan '.$input['nama_tagihan'],
            'total' => $input['pembayaran'],
            'jenis_transkeluar' => 'Tagihan',
        );

        $this->db->trans_begin();
        $update = $this->Tagihan->updateTagihan($data, $input['id_tagihan']);
        $insert = $this->Transaksi_Keluar->tambahTransaksiKeluar($trans);
        if ($this->db->trans_status() === TRUE){
            $this->db->trans_commit();
            $this->session->set_flashdata('success', 'Berhasil melakukan pembayaran tagihan.');
        }else{
            $this->db->trans_rollback();
            $this->session->set_flashdata('error', 'Gagal melakukan pembayaran tagihan.');
        }
        redirect('Pegawai/tagihan');
    }

    public function hapusTagihan($id){

        $delete = $this->Tagihan->hapusTagihan($id);
        if ($delete == TRUE){
            $this->session->set_flashdata('success', 'Berhasil menghapus data tagihan.');
        }else{
            $this->session->set_flashdata('error', 'Gagal menghapus data tagihan.');
        }
        redirect('Pegawai/tagihan');
    }

    public function pengeluaran(){
        $data['pengeluaran'] = $this->Transaksi_Keluar->getTransKeluarAll()->result();
        $this->load->view('header.php', $data);
        $this->load->view('Pegawai/pengeluaran.php');
        $this->load->view('footer.php');
    }

    public function tambahPengeluaran(){
        $input = $this->input->post();

        $data= array(
            'user_id' => $this->session->userdata('id'),
            'tgl_transkeluar' => $input['tanggal'],
            'nama_transkeluar' => $input['nama_pengeluaran'],
            'total' => $input['nominal'],
            'jenis_transkeluar' => $input['jenis_pengeluaran'],
        );

        $insert = $this->Transaksi_Keluar->tambahTransaksiKeluar($data);

        if ($insert == TRUE){
            $this->session->set_flashdata('success', 'Berhasil menambah pengeluaran.');
            redirect('Pegawai/pengeluaran');
        }else{
            $this->session->set_flashdata('error', 'Gagal menambah pengeluaran.');
            redirect('Pegawai/pengeluaran');
        }
    }

    public function editPengeluaran(){
        $input = $this->input->post();
        $data= array(
            'user_id' => $this->session->userdata('id'),
            'tgl_transkeluar' => $input['tanggal'],
            'nama_transkeluar' => $input['nama_pengeluaran'],
            'total' => $input['nominal'],
            'jenis_transkeluar' => $input['jenis_pengeluaran'],
        );

        $update = $this->Transaksi_Keluar->ubahTransaksiKeluar($input['id_pengeluaran'], $data);
        if ($update == TRUE){
            $this->session->set_flashdata('success', 'Berhasil mengubah data pengeluaran.');
        }else{
            $this->session->set_flashdata('error', 'Gagal mengubah data pengeluaran.');
        }
        redirect('Pegawai/pengeluaran');
    }

    public function hapusPengeluaran($id){

        $delete = $this->Transaksi_Keluar->hapusTransaksiKeluar($id);
        if ($delete == TRUE){
            $this->session->set_flashdata('success', 'Berhasil menghapus data pengeluaran.');
        }else{
            $this->session->set_flashdata('error', 'Gagal menghapus data pengeluaran.');
        }
        redirect('Pegawai/pengeluaran');
    }

    //Peramalan
    public function buat_peramalan(){
        $tanggal_awal = $this->Peramalan->getPeramalanAwal()->row_array();
        $date = date('Y-m-d', strtotime($tanggal_awal['periode']));

        $data['bahanbaku'] = $this->Bahanbaku->getBahanbakuKopi()->result();

        $this->load->view('header.php', $data);
        $this->load->view('Pegawai/buat_peramalan.php');
        $this->load->view('footer.php');
    }

    public function prosesPeramalan(){
        $input = $this->input->post();
        $bahanbaku = $this->Resep->getResepPerBahanbaku($input['jenis_kopi'])->row_array();
        $ses = $this->peramalanSES($input);
        $regresi = $this->peramalanRegresiLinier($input);

//        if ($ses && $regresi === "Kosong"){
//            $this->session->set_flashdata('error', 'Gagal melakukan peramalan, data transaksi kurang dari 3.');
//            redirect('Pemilik/buat_peramalan');
//        }else{
//        var_dump($ses);
        $periode = count($regresi)-1;

        $hasil[0] = $regresi[$periode];
        $hasil[1] = $ses['hasil'][$ses['indexMapeTerkecil']][$periode];

        $MAPE[0] = $regresi[$periode]['MAPE'];
        $MAPE[1] = $ses['hasil'][$ses['indexMapeTerkecil']][$periode]['MAPE'];

        $indexMapeTerkecil = array_search(min($MAPE), $MAPE);

        $data['hasil_rekomendasi'] = $hasil;
        $data['indexMapeTerkecil'] = $indexMapeTerkecil;
        $data['regresi'] = $regresi;
        $data['ses'] = $ses['hasil'][$ses['indexMapeTerkecil']];
        $data['bahanbaku'] = $bahanbaku;

        $this->load->view('header.php', $data);
        $this->load->view('Pegawai/hasil_peramalan.php');
        $this->load->view('footer.php');
//        }
    }

    public function simpanPeramalan(){
        $input = $this->input->post();

        $data = array(
            'bahanbaku_id' => $input['bahanbaku_id'],
            'tanggal' => $input['tanggal'],
            'periode' => $input['periode'],
            'metode' => $input['metode'],
            'hasil' => $input['hasil'],
            'rekomendasi_stok' => $input['rekomendasi_stok'],
            'MAPE' => $input['MAPE'],
        );

        $insert = $this->Peramalan->tambahPeramalan($data);
        if ($insert == TRUE){
            $this->session->set_flashdata('success', 'Berhasil menyimpan data peramalan.');
            redirect('Pegawai/riwayat_peramalan');
        }else{
            $this->session->set_flashdata('error', 'Gagal menyimpan peramalan.');
            redirect('Pegawai/buat_peramalan');
        }
    }

    public function riwayat_peramalan(){
        $data['peramalan'] = $this->Peramalan->getPeramalanAll()->result();
        $this->load->view('header.php', $data);
        $this->load->view('Pegawai/historyperamalan.php');
        $this->load->view('footer.php');
    }

    public function hapusPeramalan($id){
        $delete = $this->Peramalan->hapusPeramalan($id);

        if ($delete == TRUE){
            $this->session->set_flashdata('success', 'Berhasil menghapus data.');
            redirect('Pegawai/riwayat_peramalan');
        }else{
            $this->session->set_flashdata('error', 'Gagal menghapus data.');
            redirect('Pegawai/history_peramalan');
        }
    }

    public function peramalanSES($data){
        $id_bahanbaku = $data['jenis_kopi'];

        $tanggal = $data['periode'];

        //ambil tanggal paling awal
        $from = $this->Transaksi->getTransaksiPerBahanbaku($data['jenis_kopi'])->row()->tgl_trans;

        $cek = $this->Transaksi->getTransaksiQtyPerMinggu($data['jenis_kopi'], $from, $tanggal)->result_array();

        if (count($cek) < 1){
            $hasil = "Kosong";
        }else {
            //ini ambil data transaksi dari db
            $data = $this->Transaksi->getTransaksiQtyPerMinggu($data['jenis_kopi'], $from, $tanggal)->result_array();

            //ini ambil periodenya
            $periode = array();
            $awal = date('Y-m-d', strtotime($from));
            $akhir = date('Y-m-d', strtotime("+6 day", strtotime(date($awal))));
            $i = 0;
            while (date('Y-m-d', strtotime($awal)) <= date('Y-m-d', strtotime($tanggal))) {
                $periode[$i] = $awal;
                $awal = date('Y-m-d', strtotime("+7 day", strtotime(date($awal))));
                $i++;
            }

            //ini menghitung total
            $X = array();
            for ($i = 0; $i < count($periode); $i++) {
                for ($j = 0; $j < count($data); $j++) {
                    if ($periode[$i] == $data[$j]['tgl_trans']) {
                        $X[$i] = intval($data[$j]['qty']);
                        break;
                    } else {
                        $X[$i] = 0;
                    }
                }
            }

            //perhitungan SES
            $F = array();
            $alpha = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9];
            $PE = array();
            $MAPE = array();

            // perhitungan peramalan menggunakan nilai alpha mulai dari 0.1 sampai 0.9
            for ($i = 0; $i < count($alpha); $i++) {
                // inisialisasi
                $F[$i][0] = $X[0];
                $PE[$i][0] = 0;
                $e[$i][0] = 0;
                $E[$i][0] = 0;

                for ($j = 1; $j < count($periode); $j++) {
                    // perhitungan peramalan untuk periode berikutnya
                    $F[$i][$j] = ($alpha[$i] * $X[$j - 1]) + ((1-$alpha[$i]) * $F[$i][$j - 1]);

                    // menghitung nilai kesalahan persentase peramalan
                    if ($j < count($periode)-1){
                        $PE[$i][$j] = $X[$j] == 0 ? 0 : abs((($X[$j] - $F[$i][$j]) / $X[$j]) * 100);
                    }
                    $e[$i][$j] = abs($X[$j] - $F[$i][$j]);
                }

                for ($a=0; $a < count($e[$i]); $a++){
                    if ($a == (count($periode)-1)){
                        break;
                    }else{
                        $E[$i][$a] = $e[$i][$a];
                    }
//                    echo $X[$a]." - ".$F[$i][$a].". E =".$E[$i][$a]."<br>";
                }

                // menghitung rata-rata kesalahan peramalan
                if (count($periode) > 1) {
                    $MAPE[$i] = array_sum($PE[$i]) / (count($periode) - 1);
                    $MAD[$i] = array_sum($E[$i])/ count($E[$i]);
                } else {
                    $MAPE[$i] = 0;
                    $MAD[$i] = 0;
                }
            }

            // mendapatkan index alpha dengan nilai mape terkecil
            $indexMapeTerkecil = array_search(min($MAPE), $MAPE);
            $indexMADTerkecil = array_search(min($MAD), $MAD);
            $bestAlphaIndexMAPE = $alpha[$indexMapeTerkecil];
            $bestAlphaIndexMAD = $alpha[$indexMADTerkecil];

            // menyatukan semua hasil perhitungan dan menginputkan hasil peramalan periode berikutnya
            $hasil = array();
            $hasiltotal = 0.0;
            for ($j = 0; $j < count($alpha); $j++) {
                for ($i = 0; $i < count($periode); $i++) {
                    if ($i < count($periode)-1) {
                        $hasil[$j][$i] = array(
                            'id_bahanbaku' => $id_bahanbaku,
                            'metode' => "Single Exponential Smoothing Alpha ".$alpha[$j],
                            'periode' => $periode[$i],
                            'aktual' => $X[$i],
                            'hasil' => $F[$j][$i],
                            'alpha' => $alpha[$j],
                            'E' => $e[$j][$i],
                            'PE' => $PE[$j][$i],
                            'MAPE' => $MAPE[$j],
                            'MAD' => $MAD[$j]
                        );
                    } else {
                        $hasil[$j][$i] = array(
                            'id_bahanbaku' => $id_bahanbaku,
                            'metode' => "Single Exponential Smoothing Alpha ".$alpha[$j],
                            'periode' => $periode[$i],
                            'aktual' => 0,
                            'hasil' => $F[$j][$i],
                            'alpha' => $alpha[$j],
                            'E' => $e[$j][$i],
                            'PE' => 0,
                            'MAPE' => $MAPE[$j],
                            'MAD' => $MAD[$j]
                        );
                        $total[$i] = $F[$indexMapeTerkecil][$i];
                    }
                }
            }
            $dataHasil = array(
                'hasil' => $hasil,
                'alpha' => $alpha,
                'bestAlphaIndexMAPE' => $bestAlphaIndexMAPE,
                'indexMapeTerkecil' => $indexMapeTerkecil,
                'bestAlphaIndexMAD' => $bestAlphaIndexMAD,
                'indexMADTerkecil' => $indexMADTerkecil,
            );
        }

        return $dataHasil;
    }

    public function peramalanRegresiLinier($data){
        $id_bahanbaku = $data['jenis_kopi'];
        $tanggal = $data['periode'];

        //ambil tanggal paling awal
        $from = $this->Transaksi->getTransaksiPerBahanbaku($data['jenis_kopi'])->row()->tgl_trans;

        $cek = $this->Transaksi->getTransaksiQtyPerMinggu($data['jenis_kopi'], $from, $tanggal)->result_array();

//        if (count($cek) < 3){
//            $hasil = "Kosong";
//        }else {
        //ini ambil data transaksi dari db
        $data = $this->Transaksi->getTransaksiQtyPerMinggu($data['jenis_kopi'], $from, $tanggal)->result_array();

        //ini ambil periodenya
        $periode = array();
        $awal = date('Y-m-d', strtotime($from));
        $akhir = date('Y-m-d', strtotime("+6 day", strtotime(date($awal))));
        $i = 0;
        while (date('Y-m-d', strtotime($awal)) <= date('Y-m-d', strtotime($tanggal))) {
            $periode[$i] = $awal;
            $awal = date('Y-m-d', strtotime("+7 day", strtotime(date($awal))));
            $i++;
        }

        //ini menghitung total
        $X = array();
        for ($i = 0; $i < count($periode); $i++) {
            for ($j = 0; $j < count($data); $j++) {
                if ($periode[$i] == $data[$j]['tgl_trans']) {
                    $X[$i] = intval($data[$j]['qty']);
                    break;
                } else {
                    $X[$i] = 0;
                }
            }
        }

        //ini menghitung Xperiode
        $Xp = array();
        for ($i = 0; $i < count($periode); $i++) {
            $Xp[$i] = 1 + $i;
        }
        for ($i = 0; $i < count($Xp); $i++) {
            $x[$i] = $Xp[$i];
            $Y[$i] = $X[$i];
            $XX[$i] = $Xp[$i] * $Xp[$i];
            $XY[$i] = $Xp[$i] * $X[$i];
            for ($j = 0; $j < count($x); $j++) {
                if ($Xp[$j] == $x[$i]) {
                    $sumX[$i] = array_sum($x);
                    $sumY[$i] = array_sum($Y);
                    $sumXX[$i] = array_sum($XX);
                    $sumXY[$i] = array_sum($XY);
                    $countX[$i] = count($x);
                    $countXX[$i] = count($XX);
                    $countXY[$i] = count($XY);
                }
            }
        }

        //perhitungan Regresi Linier
        $F = array();
        $b = array();
        $a = array();
        $PE = array();
        $MAPE = array();

        // perhitungan peramalan menggunakan nilai alpha mulai dari 0.1 sampai 0.9
        for ($i = 0; $i < count($periode); $i++) {
            // inisialisasi
//                $F[0] = $PE[0] = $e[0] = "-";
//                $F[1] = $PE[1] = $e[1] = "-";

//                if ($i >= 2) {

            // perhitungan peramalan untuk periode berikutnya
//                    $b[$i] = (($countX[$i - 1] * $sumXY[$i - 1]) - ($sumX[$i - 1] * $sumY[$i - 1])) / (($countX[$i - 1] * $sumXX[$i - 1]) - ($sumX[$i - 1] * $sumX[$i - 1]));
//                    $a[$i] = (($sumY[$i - 1] * $sumXX[$i - 1]) - ($sumX[$i - 1] * $sumXY[$i - 1])) / (($countX[$i - 1] * $sumXX[$i - 1]) - ($sumX[$i - 1] * $sumX[$i - 1]));
            $b[$i] = 2.05;
            $a[$i] = 106.02;
            $F[$i] = $a[$i] + ($b[$i] * $Xp[$i]);

            // menghitung nilai kesalahan persentase peramalan
            if ($i == (count($periode) - 1)) {
//                        $PE[$i] = 0;
                $e[$i] = abs($X[$i] - $F[$i]);
            } else {
                $PE[$i] = $X == 0 ? 0 : abs((($X[$i] - $F[$i]) / $X[$i]) * 100);
                $e[$i] = abs($X[$i] - $F[$i]);
            }
//                }
        }

        for ($a = 0; $a <= count($e)-2; $a++) {
            $E[$a] = $e[$a];
        }

        // menghitung rata-rata kesalahan peramalan
        if (count($periode) > 1) {
            $MAPE = array_sum($PE) / (count($periode)-1);
            $MAD = array_sum($E) / count($E);
        } else {
            $MAPE = 0;
            $MAD = 0;
        }

        // menyatukan semua hasil perhitungan dan menginputkan hasil peramalan periode berikutnya
        $hasil = array();
        $hasiltotal = 0.0;
        for ($i = 0; $i < count($periode); $i++) {
            if ($i < count($periode)-1) {
                $hasil[$i] = array(
                    'id_bahanbaku' => $id_bahanbaku,
                    'metode' => "Regresi Linier Sederhana",
                    'periode' => $periode[$i],
                    'aktual' => $X[$i],
                    'hasil' => $F[$i],
                    'E' => $e[$i],
                    'PE' => $PE[$i],
                    'MAPE' => $MAPE,
                    'MAD' => $MAD
                );
            } else {
                $hasil[$i] = array(
                    'id_bahanbaku' => $id_bahanbaku,
                    'metode' => "Regresi Linier Sederhana",
                    'periode' => $periode[$i],
                    'aktual' => 0,
                    'hasil' => $F[$i],
                    'E' => $e[$i],
                    'PE' => 0,
                    'MAPE' => $MAPE,
                    'MAD' => $MAD
                );
                $total[$i] = $F[$i];
            }
        }
//        }
        return $hasil;
    }

    //Akun
    public function daftar_pegawai(){
        $data['pegawai'] = $this->User->getDataPegawaiAll()->result();
        $this->load->view('header.php', $data);
        $this->load->view('Pegawai/daftarpegawai.php');
        $this->load->view('footer.php');
    }

    public function tambahPegawai(){
        $input = $this->input->post();

        if ($input['penjualan'] == "on"){
            $penjualan = 1;
        }else{
            $penjualan = 0;
        }
        if ($input['keuangan'] == "on"){
            $keuangan = 1;
        }else{
            $keuangan = 0;
        }
        if ($input['stok'] == "on"){
            $stok = 1;
        }else{
            $stok = 0;
        }

        //proses konfigurasi gambar
        $config['upload_path']          = './assets/FotoProfil/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
        $config['max_size']             = 0;

        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload('gambar');
        $uploaddata = $this->upload->data();
        $nama = $uploaddata['file_name'];

        if ($nama != ""){
            $gambar = $nama;
        }else{
            $gambar = NULL;
        }

        $data = array(
            'nama' => $input['nama'],
            'username' => $input['username'],
            'password' => $input['password'],
            'jabatan' => 2,
            'penjualan' => $penjualan,
            'stok' => $stok,
            'keuangan' => $keuangan,
            'foto' => $gambar,
        );
        $insert = $this->User->tambahUser($data);

        if($insert == TRUE){
            $this->session->set_flashdata('success', 'Berhasil menambahkan data.');
            redirect('Pegawai/daftar_pegawai');
        }else{
            $this->session->set_flashdata('error', 'Gagal menambahkan data. Silahkan ulangi lagi.');
            redirect('Pegawai/daftar_pegawai');
        }
    }

    public function ubahPegawai(){
        $input = $this->input->post();

        if ($input['penjualan'] == "on"){
            $penjualan = 1;
        }else{
            $penjualan = 0;
        }
        if ($input['keuangan'] == "on"){
            $keuangan = 1;
        }else{
            $keuangan = 0;
        }
        if ($input['stok'] == "on"){
            $stok = 1;
        }else{
            $stok = 0;
        }

        //proses konfigurasi gambar
        $config['upload_path']          = './assets/FotoProfil/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
        $config['max_size']             = 0;

        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload('gambar');
        $uploaddata = $this->upload->data();
        $nama = $uploaddata['file_name'];

        if ($nama != ""){
            $gambar = $nama;
            unlink("assets/FotoProfil/".$input['gambar_lama']);
        }else{
            $gambar = $input['gambar_lama'];
        }

        $data = array(
            'nama' => $input['nama'],
            'username' => $input['username'],
            'password' => $input['password'],
            'jabatan' => 2,
            'penjualan' => $penjualan,
            'stok' => $stok,
            'keuangan' => $keuangan,
            'foto' => $gambar,
        );
        $insert = $this->User->ubahUser($data, $input['id']);

        if($insert == TRUE){
            $this->session->set_flashdata('success', 'Berhasil mengubah data.');
            redirect('Pegawai/daftar_pegawai');
        }else{
            $this->session->set_flashdata('error', 'Gagal mengubah data. Silahkan ulangi lagi.');
            redirect('Pegawai/daftar_pegawai');
        }
    }

    public function ubah_akun(){
        $id = $this->session->userdata('id');
        $data['akun'] = $this->User->getUserPerId($id)->row_array();

        $this->load->view('header.php', $data);
        $this->load->view('Pegawai/ubahakun.php');
        $this->load->view('footer.php');
    }

    public function ubahAkun(){
        $input = $this->input->post();

        //proses konfigurasi gambar
        $config['upload_path']          = './assets/FotoProfil/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
        $config['max_size']             = 0;

        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload('gambar');
        $uploaddata = $this->upload->data();
        $nama = $uploaddata['file_name'];

        if ($nama != ""){
            $gambar = $nama;
            unlink("assets/FotoProfil/".$input['gambar_lama']);
        }else{
            $gambar = $input['gambar_lama'];
        }

        $data = array(
            'nama' => $input['nama'],
            'username' => $input['username'],
            'password' => $input['password'],
            'foto' => $gambar,
        );
        $insert = $this->User->ubahUser($data, $input['id']);

        if($insert == TRUE){
            $this->session->set_flashdata('success', 'Berhasil mengubah data.');
            redirect('Pegawai/ubah_akun');
        }else{
            $this->session->set_flashdata('error', 'Gagal mengubah data. Silahkan ulangi lagi.');
            redirect('Pegawai/ubah_akun');
        }
    }

}