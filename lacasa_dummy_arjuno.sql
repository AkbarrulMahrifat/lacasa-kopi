-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 17, 2019 at 07:08 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lacasa`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahanbaku`
--

CREATE TABLE `bahanbaku` (
  `id_bahanbaku` int(11) NOT NULL,
  `nama_bahanbaku` varchar(30) NOT NULL,
  `harga_bahanbaku` int(11) NOT NULL,
  `satuan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bahanbaku`
--

INSERT INTO `bahanbaku` (`id_bahanbaku`, `nama_bahanbaku`, `harga_bahanbaku`, `satuan`) VALUES
(1, 'Susu', 22, 'gr'),
(2, 'Arabica Ijen Raung', 280, 'gr'),
(3, 'Arabica Bedhag', 200, 'gr'),
(4, 'Arabica Arjuno', 280, 'gr'),
(5, 'Robusta Sitoot Baban', 100, 'gr'),
(6, 'Robusta Gendhing', 120, 'gr');

-- --------------------------------------------------------

--
-- Table structure for table `detail_trans`
--

CREATE TABLE `detail_trans` (
  `id_detailtrans` int(11) NOT NULL,
  `trans_id` int(11) DEFAULT NULL,
  `menu_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_trans`
--

INSERT INTO `detail_trans` (`id_detailtrans`, `trans_id`, `menu_id`, `qty`, `status`) VALUES
(1, 1, 5, 103, 2),
(4, 3, 5, 148, 2),
(5, 4, 5, 99, 2),
(6, 5, 5, 107, 2),
(7, 6, 5, 143, 2),
(8, 7, 5, 101, 2),
(9, 8, 5, 107, 2),
(10, 9, 5, 106, 2),
(11, 10, 5, 157, 2),
(12, 11, 5, 137, 2),
(13, 12, 5, 99, 2),
(14, 13, 5, 186, 2),
(15, 14, 5, 144, 2),
(16, 15, 5, 135, 2),
(17, 16, 5, 128, 2),
(18, 17, 5, 177, 2),
(19, 18, 5, 74, 2),
(20, 19, 5, 128, 2),
(21, 20, 5, 159, 2),
(22, 21, 5, 152, 2),
(23, 22, 5, 179, 2),
(24, 23, 5, 113, 2);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(30) NOT NULL,
  `harga` int(11) NOT NULL,
  `gambar` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `nama_menu`, `harga`, `gambar`) VALUES
(1, 'Arabica Bedhag Tubruk', 8000, 'kopi1.jpg'),
(2, 'Arabica Bedhag Tubruk Susu', 10000, 'kopi2.jpg'),
(3, 'Robusta Gendhing Tubruk', 5000, 'kopi3.jpg'),
(4, 'Robusta Gendhing Tubruk Susu', 7000, 'kopi4.jpg'),
(5, 'Arabica Arjuno Tubruk', 8000, 'kopi5.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `peramalan`
--

CREATE TABLE `peramalan` (
  `id_peramalan` int(11) NOT NULL,
  `bahanbaku_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `periode` varchar(225) NOT NULL,
  `metode` varchar(225) NOT NULL,
  `aktual` decimal(10,2) NOT NULL,
  `hasil` decimal(10,2) NOT NULL,
  `PE` decimal(10,2) NOT NULL,
  `MAPE` decimal(10,2) NOT NULL,
  `MAD` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `resep`
--

CREATE TABLE `resep` (
  `id_resep` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `bahanbaku_id` int(11) NOT NULL,
  `takaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resep`
--

INSERT INTO `resep` (`id_resep`, `menu_id`, `bahanbaku_id`, `takaran`) VALUES
(1, 1, 3, 15),
(2, 2, 3, 15),
(3, 2, 1, 40),
(4, 3, 6, 15),
(5, 4, 6, 15),
(6, 4, 1, 40),
(7, 5, 4, 15);

-- --------------------------------------------------------

--
-- Table structure for table `stok`
--

CREATE TABLE `stok` (
  `id_stok` int(11) NOT NULL,
  `bahanbaku_id` int(11) NOT NULL,
  `tgl_beli` date NOT NULL,
  `tgl_kadaluarsa` date NOT NULL,
  `jumlah_beli` int(11) NOT NULL,
  `sisa` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok`
--

INSERT INTO `stok` (`id_stok`, `bahanbaku_id`, `tgl_beli`, `tgl_kadaluarsa`, `jumlah_beli`, `sisa`, `status`) VALUES
(1, 1, '2019-05-21', '2019-06-11', 1000, 800, 1),
(4, 2, '2019-05-21', '2019-06-11', 250, 250, 1),
(5, 3, '2019-05-21', '2019-06-11', 250, 175, 1),
(6, 4, '2019-05-21', '2019-06-11', 250, 250, 1),
(7, 5, '2019-05-21', '2019-06-11', 250, 250, 1),
(8, 6, '2019-05-21', '2019-06-11', 250, 175, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tagihan`
--

CREATE TABLE `tagihan` (
  `id_tagihan` int(11) NOT NULL,
  `nama_tagihan` varchar(30) NOT NULL,
  `nominal` int(11) NOT NULL,
  `sisa` int(11) NOT NULL,
  `tgl_jatuhtempo` date NOT NULL,
  `jenis_tagihan` varchar(20) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tagihan`
--

INSERT INTO `tagihan` (`id_tagihan`, `nama_tagihan`, `nominal`, `sisa`, `tgl_jatuhtempo`, `jenis_tagihan`, `status`) VALUES
(1, 'Bayar Listrik', 200000, 0, '2019-06-06', 'Bulanan', 1),
(2, 'Sewa Rumah', 19000000, 16000000, '2019-11-30', 'Tahunan', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_trans` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tgl_trans` date NOT NULL,
  `total` int(11) NOT NULL,
  `bayar` int(11) NOT NULL,
  `kembali` int(11) NOT NULL,
  `nomeja` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_trans`, `user_id`, `tgl_trans`, `total`, `bayar`, `kembali`, `nomeja`, `status`) VALUES
(1, 1, '2019-01-01', 8000, 10000, 2000, 2, 2),
(3, 1, '2019-01-08', 30000, 30000, 0, 2, 2),
(4, 1, '2019-01-15', 15000, 15000, 0, 3, 2),
(5, 1, '2019-01-22', 15000, 20000, 5000, 1, 2),
(6, 1, '2019-01-29', 10000, 10000, 0, 8, 2),
(7, 1, '2019-02-05', 20000, 20000, 0, 4, 2),
(8, 1, '2019-02-12', 10, 10, 10, 2, 2),
(9, 1, '2019-02-19', 1, 1, 1, 1, 2),
(10, 1, '2019-02-26', 1, 1, 2, 1, 2),
(11, 1, '2019-03-05', 1, 1, 1, 1, 2),
(12, 1, '2019-03-12', 1, 1, 1, 1, 2),
(13, 1, '2019-03-19', 1, 1, 1, 1, 2),
(14, 1, '2019-03-26', 1, 1, 1, 1, 2),
(15, 1, '2019-04-02', 1, 1, 1, 1, 2),
(16, 1, '2019-04-09', 1, 1, 1, 1, 2),
(17, 1, '2019-04-16', 1, 1, 1, 1, 2),
(18, 1, '2019-04-23', 1, 1, 1, 1, 2),
(19, 1, '2019-04-30', 1, 1, 1, 1, 2),
(20, 1, '2019-05-07', 2, 2, 2, 2, 2),
(21, 1, '2019-05-14', 2, 2, 2, 2, 2),
(22, 1, '2019-05-21', 2, 2, 2, 2, 2),
(23, 1, '2019-05-28', 2, 2, 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_keluar`
--

CREATE TABLE `transaksi_keluar` (
  `id_transkeluar` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tgl_transkeluar` date NOT NULL,
  `nama_transkeluar` varchar(225) NOT NULL,
  `total` int(11) NOT NULL,
  `jenis_transkeluar` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_keluar`
--

INSERT INTO `transaksi_keluar` (`id_transkeluar`, `user_id`, `tgl_transkeluar`, `nama_transkeluar`, `total`, `jenis_transkeluar`) VALUES
(1, 1, '2019-05-21', 'Belanja Susu', 22000, 'Belanja'),
(3, 1, '2019-05-21', 'Belanja Arabica Ijen Raung', 70000, 'Belanja'),
(4, 1, '2019-05-21', 'Belanja Arabica Bedhag', 50000, 'Belanja'),
(5, 1, '2019-05-21', 'Belanja Arabica Arjuno', 70000, 'Belanja'),
(6, 1, '2019-05-21', 'Belanja Robusta Sitoot Baban', 25000, 'Belanja'),
(7, 1, '2019-05-21', 'Belanja Robusta Gendhing', 30000, 'Belanja'),
(8, 1, '2019-06-07', 'Pembayaran tagihan Bayar Listrik', 200000, 'Tagihan'),
(9, 1, '2019-06-08', 'Pembayaran tagihan Sewa Rumah', 3000000, 'Tagihan');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jabatan` tinyint(4) NOT NULL,
  `penjualan` tinyint(4) NOT NULL,
  `stok` tinyint(4) NOT NULL,
  `keuangan` tinyint(4) NOT NULL,
  `foto` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `jabatan`, `penjualan`, `stok`, `keuangan`, `foto`) VALUES
(1, 'pemilik', 'pemilik', 'Owner Lacasa', 1, 1, 1, 1, 'pemilik.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahanbaku`
--
ALTER TABLE `bahanbaku`
  ADD PRIMARY KEY (`id_bahanbaku`);

--
-- Indexes for table `detail_trans`
--
ALTER TABLE `detail_trans`
  ADD PRIMARY KEY (`id_detailtrans`),
  ADD KEY `trans_id` (`trans_id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `peramalan`
--
ALTER TABLE `peramalan`
  ADD PRIMARY KEY (`id_peramalan`),
  ADD KEY `bahanbaku_id` (`bahanbaku_id`);

--
-- Indexes for table `resep`
--
ALTER TABLE `resep`
  ADD PRIMARY KEY (`id_resep`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `bahanbaku_id` (`bahanbaku_id`);

--
-- Indexes for table `stok`
--
ALTER TABLE `stok`
  ADD PRIMARY KEY (`id_stok`),
  ADD KEY `bahanbaku_id` (`bahanbaku_id`);

--
-- Indexes for table `tagihan`
--
ALTER TABLE `tagihan`
  ADD PRIMARY KEY (`id_tagihan`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_trans`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `transaksi_keluar`
--
ALTER TABLE `transaksi_keluar`
  ADD PRIMARY KEY (`id_transkeluar`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bahanbaku`
--
ALTER TABLE `bahanbaku`
  MODIFY `id_bahanbaku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `detail_trans`
--
ALTER TABLE `detail_trans`
  MODIFY `id_detailtrans` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `peramalan`
--
ALTER TABLE `peramalan`
  MODIFY `id_peramalan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `resep`
--
ALTER TABLE `resep`
  MODIFY `id_resep` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stok`
--
ALTER TABLE `stok`
  MODIFY `id_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tagihan`
--
ALTER TABLE `tagihan`
  MODIFY `id_tagihan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_trans` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `transaksi_keluar`
--
ALTER TABLE `transaksi_keluar`
  MODIFY `id_transkeluar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_trans`
--
ALTER TABLE `detail_trans`
  ADD CONSTRAINT `detail_trans_ibfk_1` FOREIGN KEY (`trans_id`) REFERENCES `transaksi` (`id_trans`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_trans_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id_menu`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `resep`
--
ALTER TABLE `resep`
  ADD CONSTRAINT `resep_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id_menu`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `resep_ibfk_2` FOREIGN KEY (`bahanbaku_id`) REFERENCES `bahanbaku` (`id_bahanbaku`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `stok`
--
ALTER TABLE `stok`
  ADD CONSTRAINT `stok_ibfk_1` FOREIGN KEY (`bahanbaku_id`) REFERENCES `bahanbaku` (`id_bahanbaku`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi_keluar`
--
ALTER TABLE `transaksi_keluar`
  ADD CONSTRAINT `transaksi_keluar_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
